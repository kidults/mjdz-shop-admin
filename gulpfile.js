const gulp = require('gulp');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const cssnano = require('gulp-cssnano');
const concat = require('gulp-concat');
const source = require('vinyl-source-stream');
const shim = require('browserify-shim');
const less = require('gulp-less');
// 编译并压缩js
gulp.task('convertJS', function(){
    return gulp.src('src/**/*.js')
        .pipe(babel({
            presets: ['es2015','react',"stage-0"]
        }))
        .pipe(uglify())
        .pipe(gulp.dest('dist'))
})

gulp.task('imgTask', function(){
    return gulp.src('src/img/*.*')
        .pipe(gulp.dest('dist/css/default/img'))
})

gulp.task('trilobitaDesigner', function(){
    return gulp.src('node_modules/trilobita-d/**')
        .pipe(gulp.dest('dist/trilobita-d'))
})

gulp.task('buildLess', function () {
    return gulp.src('src/less/**/*.less')
        .pipe(less())
        .pipe(gulp.dest('dist/less'));
});

// 合并并压缩css
gulp.task('convertCSS', function(){
    return gulp.src('dist/less/**/*.css')
        .pipe(concat('./default/app.css'))
        .pipe(cssnano({reduceIdents: false}))
        .pipe(rename(function(path){
            path.basename += '.min';
        }))
        .pipe(gulp.dest('dist/css'));
})

// 监视文件变化，自动执行任务
gulp.task('watch', function(){
    gulp.watch('src/less/**/*.less', ['buildLess']);
    gulp.watch('dist/less/**/*.css', ['convertCSS']);
    gulp.watch('src/**/*.js', ['convertJS']);
    gulp.watch('src/img/**/*.*', ['imgTask']);
})


gulp.task('start', ['imgTask','buildLess','convertJS', 'convertCSS', 'watch']);
