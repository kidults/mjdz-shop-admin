/**
 * Created by quxiangqian on 2018/7/13.
 */
import {Dialog} from 'trilobita-d/control/Dialog';
import {Form} from 'trilobita-d/form/Form';
import {Button} from 'trilobita-d/control/Button';
import {TextField} from "trilobita-d/form/TextField";
import {DaoFactory} from "config/DaoFactory"
import {WindowManager} from "trilobita-d/control/WindowManager";
export class Login extends React.Component{

    state={
        dataSource:{
            username:{value:"",validate:"null"},
            password:{value:"",validate:"null"}
        }
    }

    constructor(){
        super();
        DaoFactory.AdminUser.Login.addSuccess(this.LoginSuccess)
    }
    LoginSuccess=(data)=>{
        let d=data.data;
        if(data.code===200){
            sessionStorage.setItem("isLogin",true);
            sessionStorage.setItem("userInfo",JSON.stringify({...d}))
            if(this.props.goto){
                this.props.goto();
            }
        }else{
            this.win.alert(d);
        }
    }

    formContent(props){

        let item=props.dataSource;

        return (<table className="form-layout">
            <tr>
                <td>用户名：</td>
                <td> <TextField bind={item} type="text" placeHolder="请输入用户名" name="username"/></td>
            </tr>
            <tr>
                <td>密码：</td>
                <td><TextField bind={item} type="password" placeHolder="请输入密码" name="password"/></td>
            </tr>

        </table>)
    }
    login=()=>{
        if(this.refs.form.isValiData()){
            DaoFactory.AdminUser.Login.data=this.refs.form.getData();
            DaoFactory.AdminUser.Login.load();
        }
    }
    render(){
        let FormContent=this.formContent
        return <div className="login abs-layout">
            <WindowManager ref={(wm)=>this.win=wm}/>
            <div className="rect">
                <div className="subrect">
                    <div className="header">名酒定制分销系统</div>

                    <Form dataSource={this.state.dataSource} ref="form">
                        <FormContent/>
                        <div className="form-command">
                            <Button css="btn-primary"  style={{width: "-webkit-fill-available",margin:" .5rem 7rem"}} type="button" onClick={(e)=>{
                                this.login();
                            }}  margin="mg1-right">登 录</Button>

                        </div>
                    </Form>

                </div>
            </div>
        </div>
    }
}