/**
 * Created by quxiangqian on 2018/6/17.
 */
import {Dialog} from 'trilobita-d/control/Dialog';
import {Form} from 'trilobita-d/form/Form';
import {Button} from 'trilobita-d/control/Button';
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {Radio} from "trilobita-d//form/Radio";
import {DaoFactory} from "config/DaoFactory"


export class AuditDialog extends React.Component{

    state={
        dataSource:{
            idcardId:{value:""},
            status:{value:"3",validate:"null"},
            remark:{value:"",func:(item)=>{

                if(this.state.dataSource.status.value==="2"&&item.value===""){
                    item.showError("标示异常，备注被能为空");
                    //alert("标示异常，备注被能为空")
                    return false;
                }else{
                    item.default();
                    return true;
                }
            }}
        }
    }
    constructor(){
        super();
        DaoFactory.Member.Audit.addSuccess(this.memberAuditSuccess)
    }
    memberAuditSuccess=(data)=>{
        if(this.props.saveAfter){
            this.props.saveAfter();
            if(this.hide){
                this.hide();
            }

        }
    }

    show=(row)=>{
        this.refs.dialog.show();
        for(let r in row){
            if(this.state.dataSource[r]){
                this.state.dataSource[r].value=row[r]+"";
            }

        }
        this.setState( {dataSource:this.state.dataSource});
    }

    hide=()=>{
        if(this.refs.dialog){
            this.refs.dialog.hide();
        }

    }


    handleChange(e){

    }


    formContent(props){
        let List=(props)=>{
            return <div onClick={()=>props.comBoxVal(props.val,props.children)}>{props.children}</div>
        }
        let item=props.dataSource;

        return (<table className="form-layout">
            <tr>
                <td>是否审核：</td>
                <td> <Radio bind={item} name="status" value="3" text="已审核"/><Radio bind={item} name="status" value="2" text="异常"/><Radio bind={item} name="status" value="1" text="未审核"/></td>
            </tr>
            <tr>
                <td>备注：</td>
                <td><TextField bind={item} name="remark"/></td>
            </tr>

        </table>)
    }

    validate=()=>{
        if(this.refs.form.isValiData()){

            DaoFactory.Member.Audit.data=this.refs.form.getData();
            DaoFactory.Member.Audit.load();
        }

    }

    render(){

        let FormContent=this.formContent;
        return (
            <Dialog title="实名认证审核" ref="dialog" {...this.props} >
                <Form dataSource={this.state.dataSource} ref="form">
                    <FormContent/>
                    <div className="form-command">
                        <Button css="btn-primary"  type="button" onClick={(e)=>{this.validate()}} margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={this.hide} margin="mg1-right">关闭</Button>
                    </div>
                </Form>
            </Dialog>
        )
    }
}