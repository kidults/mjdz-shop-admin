/**
 * Created by quxiangqian on 2018/6/5.
 */
import {DataGrid} from 'trilobita-d/control/DataGrid'
import {Button} from "trilobita-d/control/Button";
import {Panel} from "trilobita-d/control/Panel";
import {Hbox} from "trilobita-d/layout/Hbox";
import {Form} from 'trilobita-d/form/Form';
import {AuditDialog} from "./AuditDialog";
import {Card} from "trilobita-d/control/Card";
import {NaviCard} from "trilobita-d/control/NaviCard";
import {Vbox} from "trilobita-d/layout/Vbox";
import {PerPageBar} from "trilobita-d/control/PerPageBar";
import {WindowManager} from "trilobita-d/control/WindowManager";
import {Popover} from "trilobita-d/control/Popover"
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {DaoFactory} from "config/DaoFactory"
import {Dialog} from 'trilobita-d/control/Dialog';
import {ToolTip} from "trilobita-d/control/ToolTip"


export default class IdCard extends React.Component{

    constructor(){
        super();
        DaoFactory.Member.IdCardFind.addSuccess(this.idCardFind_Success)
        this.loadIdCardlist();
    }
    state={
        totalRecord:0,
        dataSource:[

        ],
        idCardDetail:{
            name:'',
            fphto:'',
            bphto:'',
        },
        filterForm:{
            keyword:{value:""},
            pageNumber:{value:1},
            pageSize:{value:15},
        }
    }

    idCardFind_Success=(data)=>{
        //console.log(data.data);
        this.state.totalRecord=data.totalRecord;
        this.state.dataSource=data.data;
        this.setState({...this.state.dataSource});
    }

    loadIdCardlist=()=>{
        let data={};
        for(let key in this.state.filterForm){
            data[key]=this.state.filterForm[key].value;
        }

        DaoFactory.Member.IdCardFind.data=data
        DaoFactory.Member.IdCardFind.load();
    }

    getCloumn=()=>{
        return [
            {name:'idcardId',title:'编号',width:'120px'},

            {name:'uname',title:'用户名',width:'150px'},
            {name:'createTime',title:'申请时间',width:'150px'},

            {name:'name',title:'身份证姓名',width:'150px'},
            {name:'status',title:'审核状态',width:'300px',render:(row)=>{
                let s="";
                if(row.status===1){
                    s="待审核"
                }
                if(row.status===2){
                    s="异常"
                }
                if(row.status===3){
                    s="通过"
                }
                return<span>[{s}]&nbsp;{row.remark}&nbsp; <a href="javascript:void(0)" onClick={(e)=>{
                    this.auditDialog.show(row);
                  }
                }>编辑</a></span>
            }},
            {name:'status',title:'身份证照片',width:'120px',render:(row)=>{
                let idcardBack=row.idcardBack;
                let idcardFront=row.idcardFront;
                let str={};
                if(idcardBack!==""&&idcardFront!==""){
                    str=<a href="javascript:void(0)" onClick={(e)=>{
                        this.state.idCardDetail.name=row.name;
                        this.state.idCardDetail.fphto=idcardFront;
                        this.state.idCardDetail.bphto=idcardBack;
                        this.setState({...this.state});
                        this.idCardDetailDialog.show();

                    }
                    }>查看照片</a>
                }else{
                    str="未提供完整"
                }

                return <span>{str}</span>
            }},
            {name:'idcardNo',title:'身份证号码',width:'250px'},
            {name:'tel',title:'手机',width:'220px'},



        ];
    }


    componentDidMount(){

    }

    formContent(props){

        let item=props.dataSource;
        return (<table className="form-layout" style={{minWidth:'297px',margin:0,marginBottom:'1rem'}}>
            <tr>
                <td>关键词：<ToolTip text="请输入手机号码、用户名、姓名、身份证的任意关键词"><i class="fas fa-info-circle" style={{fontSize:"1rem"}}></i></ToolTip></td>
                <td><TextField bind={item} name="keyword"  ></TextField></td>
            </tr>
        </table>)
    }



    render(){
        let FormContent=this.formContent;
        return <Vbox css="abs-layout">
            <Popover title="筛选" ref={(c)=>{this.filterForm=c}} body={
                <Form dataSource={this.state.filterForm} style={{margin:0}} >
                    <FormContent/>
                    <div className="form-command">
                        <Button css="btn-primary"   type="button"  onClick={()=>{
                            this.loadIdCardlist();
                            this.filterForm.hide();
                        }}  margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={()=>this.filterForm.hide()} margin="mg1-right">关闭</Button>
                    </div>
                </Form>
            } pos="left-top">
                <i class="fas fa-filter" style={{fontSize:16,color:"#666",right:'1rem',top:'7rem',borderRadius:'0.5rem', border:"solid 1px #ccc",padding:'0.5rem', position:"fixed",cursor:"pointer"}}></i>
            </Popover>
            <AuditDialog ref={(dialog)=>this.auditDialog=dialog} saveAfter={()=>{
                this.loadIdCardlist();
            }}/>
            <WindowManager ref="wm"/>

            <Dialog title={"["+this.state.idCardDetail.name+"]身份证"} ref={(dialog)=>this.idCardDetailDialog=dialog} {...this.props} >
                <div style={{margin:"1rem"}}>
                <div style={{lineHeight:2.0}}>身份证正面</div>
                <img src={this.state.idCardDetail.fphto} style={{width:350,height:250}}/>
                <div style={{lineHeight:2.0}}>身份证背面</div>
                <img src={this.state.idCardDetail.bphto} style={{width:350,height:250}}/>
                </div>
            </Dialog>

            <NaviCard title="实名认证" dataSource={["首页","会员管理","实名认证"]} description="根据条件查询会员实名认证情况"></NaviCard>

            <Card css="mg4" style={{height:"100%"}}>
                <Vbox css="abs-layout">
                     <DataGrid ref="dg" cloumn={this.getCloumn()} dataSource={this.state.dataSource}></DataGrid>
                     <PerPageBar  pageSize={this.state.filterForm.pageSize.value}
                                  total={this.state.totalRecord}
                                  goTo={(index,start,end,pagesize)=>{
                                      this.state.filterForm.pageNumber.value=index;
                                      this.state.filterForm.pageSize.value=pagesize;
                                      this.loadIdCardlist();
                                  }
                                 }
                                 style={{minHeight:"2.4rem",marginTop:'1rem'}}/>
                </Vbox>
            </Card>
        </Vbox>
    }
}