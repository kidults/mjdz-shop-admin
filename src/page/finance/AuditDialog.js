/**
 * Created by quxiangqian on 2018/6/17.
 */
import {Dialog} from 'trilobita-d/control/Dialog';
import {Form} from 'trilobita-d/form/Form';
import {Button} from 'trilobita-d/control/Button';
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {Radio} from "trilobita-d/form/Radio";
import {CheckBox} from "../comm/CheckBox";
import {DaoFactory} from "config/DaoFactory"


export class AuditDialog extends React.Component{

    state={
        systemRate:0,
        commissionRate1:0,
        commissionRate2:0,
        dataSource:{
            id:{value:""},
            memberId:{value:""},
            state:{value:""},
            operatorId:{value:""},
            operatorUser:{value:""},
            remark:{value:"",func:(item)=>{

                if(this.state.dataSource.state.value==="2"&&item.value===""){
                    item.showError("处理失败，备注被能为空");
                    //alert("标示异常，备注被能为空")
                    return false;
                }else{
                    item.default();
                    return true;
                }
            }}
        }
    }
    constructor(){
        super();
        DaoFactory.Finance.WithdrawUpdateState.addSuccess(this.saveSuccess);
    }

    saveSuccess=(data)=>{
        if(this.props.saveAfter){
            this.props.saveAfter();
            if(this.hide){
                this.hide();
            }

        }
    }

    show=(row)=>{
        this.refs.dialog.show();

        for(let r in row){
            if(this.state.dataSource[r]){
                this.state.dataSource[r].value=row[r]+"";
            }
        }
        let userinfo=JSON.parse(sessionStorage.getItem("userInfo"));
        this.state.dataSource.operatorId.value=userinfo.userid;
        this.state.dataSource.operatorUser.value=userinfo.username;
        //console.log(this.state.dataSource);
        this.setState( {dataSource:this.state.dataSource});
    }

    hide=()=>{
        this.refs.dialog.hide();
    }


    handleChange(e){

    }


    formContent=(props)=>{

        let item=props.dataSource;

        return (<table className="form-layout">

            <tr>
                <td>提现状态：</td>
                <td><Radio bind={item} name="state" value="0" text="待处理"></Radio><Radio bind={item} name="state" value="2" text="处理失败"></Radio><Radio bind={item} name="state" value="1" text="处理完成"></Radio></td>
            </tr>
            <tr>
                <td>备注：</td>
                <td><TextField bind={item} name="remark"/></td>
            </tr>


        </table>)
    }

    validate=()=>{
        if(this.refs.form.isValiData()){

            let data=this.refs.form.getData();
            console.log("提交的数据",data)
            // if(data.isAuthorized.length===0){
            //     data.isAuthorized="0";
            // }
            // DaoFactory.Dealer.Authorize.data=data;
            // DaoFactory.Dealer.Authorize.load();
            DaoFactory.Finance.WithdrawUpdateState.data=data;
            DaoFactory.Finance.WithdrawUpdateState.load();
        }

    }

    render(){

        let FormContent=this.formContent;
        return (
            <Dialog title="提现审核" ref="dialog" {...this.props} >
                <Form dataSource={this.state.dataSource} ref="form">
                    <FormContent/>
                    <div className="form-command">
                        <Button css="btn-primary"  type="button" onClick={(e)=>{this.validate()}} margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={this.hide} margin="mg1-right">关闭</Button>
                    </div>
                </Form>
            </Dialog>
        )
    }
}