/**
 * Created by quxiangqian on 2018/6/17.
 */
import {Dialog} from 'trilobita-d/control/Dialog';
import {Form} from 'trilobita-d/form/Form';
import {Button} from 'trilobita-d/control/Button';
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {Radio} from "trilobita-d/form/Radio";
import {DaoFactory} from "config/DaoFactory"
import {DataGrid} from 'trilobita-d/control/DataGrid'
import {Card} from "trilobita-d/control/Card";
import {Vbox} from "trilobita-d/layout/Vbox";
import {PerPageBar} from "trilobita-d/control/PerPageBar";
import {DatesPick} from "../comm/DatesPick"
import {TradeDetailDialog} from "./TradeDetailDialog"
export class IncomeDetailDialog extends React.Component{

    state={
        totalRecord:0,
        dataSource:[

        ],
        tradeSource:[],
        statInfo:{

        },
        idCardDetail:{
            name:'',
            fphto:'',
            bphto:'',
        },
        filterForm:{
            memberId:{value:""},
            startDate:{value:""},
            endDate:{value:""},
            tradeTypeCode:{value:""},
            pageNumber:{value:1},
            pageSize:{value:15},
        }

    }
    constructor(){
        super();
        DaoFactory.Finance.TradeRecordList.addSuccess(this.listSuccess)
        DaoFactory.Finance.TradeRecordStatInfo.addSuccess(this.statInfoSuccess)
        DaoFactory.BaseData.Tradetypes.addSuccess(this.tradeSuccess)
        DaoFactory.BaseData.Tradetypes.data={}
        DaoFactory.BaseData.Tradetypes.load();
    }

    tradeSuccess=(data)=>{
        this.state.tradeSource=data.data;

    }

    statInfoSuccess=(data)=>{
        this.state.statInfo=data.data;
        this.setState({dataSource:this.state.dataSource,statInfo:this.state.statInfo});
    }

    listSuccess=(data)=>{
        this.state.dataSource=data.data;
        this.state.totalRecord=data.totalRecord;
        DaoFactory.Finance.TradeRecordStatInfo.load();

    }

    show=(row)=>{
        this.state.filterForm.memberId.value=row.memberId;
        this.load();
        this.refs.dialog.show();
    }

    load=()=>{
        let data={};
        for(let key in this.state.filterForm){
            data[key]=this.state.filterForm[key].value;
        }
        DaoFactory.Finance.TradeRecordList.data=data;
        DaoFactory.Finance.TradeRecordStatInfo.data=data;
        DaoFactory.Finance.TradeRecordList.load();
    }

    hide=()=>{
        this.refs.dialog.hide();
    }


    handleChange(e){

    }





    getCloumn=()=>{
        return [
            {name:'tradeName',title:'交易类型',width:'120px'},
            {name:'amount',title:'收益金额',width:'150px'},
            {name:'remark',title:'备注',width:'250px'},
            {name:'createDate',title:'交易时间',width:'450px'},
            {name:"work",title:"操作",width:"150px",render:(row)=>{
                return <a href="javascript:void(0)" onClick={(e)=>{
                    this.tradeDetailDialog.show(row);
                }}>查看详细</a>
            }},
        ];
    }

    formContent=(props)=>{
        let item=props.dataSource;
        return (<table>
            <tr>
                <td>时间范围：</td>
                <td><DatesPick bind={item} name="startDate"></DatesPick></td>
                <td>-</td>
                <td><DatesPick bind={item} name="endDate"></DatesPick></td>
                <td>交易类型：</td>
                <td>
                    <ComBox readOnly={true} value={item["tradeTypeCode"].value} bind={item} name="tradeTypeCode">
                        <Option value="">全部</Option>
                        {
                            this.state.tradeSource.map((item)=><Option value={item.code}>{item.name}</Option>)
                        }

                    </ComBox>
                </td>
                <td><Button css="btn-primary" onClick={(e)=>{
                    this.load();
                }} type="button" margin="mg1-right">查 询</Button></td>
            </tr>
        </table>)
    }



    render(){

        let FormContent=this.formContent
        return (
            <Dialog title="收益情况" ref="dialog" {...this.props} >
                    <TradeDetailDialog ref={(dialog)=>this.tradeDetailDialog=dialog}></TradeDetailDialog>
                    <Card style={{height:600,width:980,position:'relative'}}>
                        <Vbox css="abs-layout">
                            <Form dataSource={this.state.filterForm} ref="form">
                                <FormContent></FormContent>
                            </Form>
                            <DataGrid ref="dg" cloumn={this.getCloumn()} dataSource={this.state.dataSource}></DataGrid>
                            <div style={{textAlign:"right"}}><h3>合计:<span>{this.state.statInfo.total}</span></h3></div>
                            <PerPageBar pageSize={this.state.filterForm.pageSize.value}
                                        total={this.state.totalRecord}
                                        goTo={(index,start,end,pagesize)=>{
                                            this.state.filterForm.pageNumber.value=index;
                                            this.state.filterForm.pageSize.value=pagesize;
                                            this.load();
                                        }
                                        }
                                        style={{minHeight:"2.4rem",marginTop:'1rem'}}/>
                        </Vbox>
                    </Card>
                    <div className="form-command">
                        <Button css="btn-primary"  type="button" margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={this.hide} margin="mg1-right">关闭</Button>
                    </div>

            </Dialog>
        )
    }
}