/**
 * Created by quxiangqian on 2018/6/5.
 */
import {DataGrid} from 'trilobita-d/control/DataGrid'
import {Button} from "trilobita-d/control/Button";
import {Panel} from "trilobita-d/control/Panel";
import {Hbox} from "trilobita-d/layout/Hbox";
import {Form} from 'trilobita-d/form/Form';
import {Card} from "trilobita-d/control/Card";
import {NaviCard} from "trilobita-d/control/NaviCard";
import {Vbox} from "trilobita-d/layout/Vbox";
import {PerPageBar} from "trilobita-d/control/PerPageBar";
import {WindowManager} from "trilobita-d/control/WindowManager";
import {Popover} from "trilobita-d/control/Popover"
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {DaoFactory} from "config/DaoFactory"
import {Dialog} from 'trilobita-d/control/Dialog';
import {AuditDialog} from "./AuditDialog"

export default class WithDrawWait extends React.Component{

    constructor(){
        super();
        DaoFactory.Finance.WithDrawList.addSuccess(this.List_Success)
        this.loadList();
    }
    state={
        totalRecord:0,
        dataSource:[

        ],
        idCardDetail:{
            name:'',
            fphto:'',
            bphto:'',
        },
        filterForm:{
            keyword:{value:""},
            state:{value:"0"},
            pageNumber:{value:1},
            pageSize:{value:15},
        }
    }

    List_Success=(data)=>{
        //console.log(data.data);
        this.state.totalRecord=data.totalRecord;
        this.state.dataSource=data.data;
        this.setState({...this.state.dataSource});
    }

    loadList=()=>{
        let data={};
        for(let key in this.state.filterForm){
            data[key]=this.state.filterForm[key].value;
        }

        DaoFactory.Finance.WithDrawList.data={...data}
        DaoFactory.Finance.WithDrawList.load();
    }

    getCloumn=()=>{
        return [
            {name:'id',title:'编号',width:'120px'},
            {name:'uname',title:'用户名',width:'150px'},
            {name:'tel',title:'电话',width:'150px'},

            {name:'idcardNo',title:'身份证号码',width:'250px'},
            {name:'name',title:'身份证姓名',width:'150px'},
            {name:'memberTypeCode',title:'经销商类型',width:'150px',render:(row)=>{

                if(row.memberTypeCode==="0000"){
                     return "平台"
                }

                if(row.memberTypeCode==="0001"){
                    return "一级经销商"
                }

                if(row.memberTypeCode==="0002"){
                    return "二级经销商"
                }
            }},

            {name:'createDate',title:'申请时间',width:'250px'},
            {name:'amount',title:'提现金额',width:'120px'},
            {name:'bankName',title:'提现银行名称',width:'150px'},
            {name:'cardNumber',title:'银行卡号',width:'150px'},
            {name:'accountBank',title:'支行名称',width:'250px'},
            {name:'accountName',title:'账户名',width:'150px'},

            {name:'work',title:'操作',width:'150px',render:(row)=>{
                return <a href="javascript:void(0)" onClick={(e)=>this.auditDialog.show(row)}>确认提现</a>
            }},

        ];
    }


    componentDidMount(){

    }

    formContent(props){

        let item=props.dataSource;
        return (<table className="form-layout" style={{minWidth:'297px',margin:0,marginBottom:'1rem'}}>
            <tr>
                <td>关键词：</td>
                <td><TextField bind={item} name="keyword"></TextField></td>
            </tr>
        </table>)
    }



    render(){
        let FormContent=this.formContent;
        return <Vbox css="abs-layout">
            <Popover title="筛选" ref={(c)=>{this.filterForm=c}} body={
                <Form dataSource={this.state.filterForm} style={{margin:0}} >
                    <FormContent/>
                    <div className="form-command">
                        <Button css="btn-primary"   type="button"  onClick={()=>{
                            this.loadList();
                            this.filterForm.hide();
                        }}  margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={()=>this.filterForm.hide()} margin="mg1-right">关闭</Button>
                    </div>
                </Form>
            } pos="left-top">
                <i class="fas fa-filter" style={{fontSize:16,color:"#666",right:'1rem',top:'7rem',borderRadius:'0.5rem', border:"solid 1px #ccc",padding:'0.5rem', position:"fixed",cursor:"pointer"}}></i>
            </Popover>

            <WindowManager ref="wm"/>
            <AuditDialog ref={(dialog)=>this.auditDialog=dialog} saveAfter={(e)=>{
                this.loadList();
             }
            } />
            <NaviCard title="经销商提现待处理" dataSource={["首页","财务管理","经销商提现待处理"]} description="根据条件查询经销商提现待处理情况"></NaviCard>

            <Card css="mg4" style={{height:"100%"}}>
                <Vbox css="abs-layout">

                    <DataGrid ref="dg" cloumn={this.getCloumn()} dataSource={this.state.dataSource}></DataGrid>
                    <PerPageBar pageSize={this.state.filterForm.pageSize.value}
                                total={this.state.totalRecord}
                                goTo={(index,start,end,pagesize)=>{
                                    this.state.filterForm.pageNumber.value=index;
                                    this.state.filterForm.pageSize.value=pagesize;
                                    this.loadList();
                                }
                                }
                                style={{minHeight:"2.4rem",marginTop:'1rem'}}/>
                </Vbox>


            </Card>
        </Vbox>
    }
}