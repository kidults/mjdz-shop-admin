/**
 * Created by quxiangqian on 2018/6/17.
 */
import {Dialog} from 'trilobita-d/control/Dialog';
import {Form} from 'trilobita-d/form/Form';
import {Button} from 'trilobita-d/control/Button';
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {Radio} from "trilobita-d/form/Radio";
import {DaoFactory} from "config/DaoFactory"
import {DataGrid} from 'trilobita-d/control/DataGrid'
import {Card} from "trilobita-d/control/Card";
import {Vbox} from "trilobita-d/layout/Vbox";
import {PerPageBar} from "trilobita-d/control/PerPageBar";
export class TradeDetailDialog extends React.Component{

    state={
        totalRecord:0,
        dataSource:[

        ],

        filterForm:{
            memberId:{value:""},
            tradeRecordId:{value:""}

        }

    }
    constructor(){
        super();
        DaoFactory.Finance.TradeDetail.addSuccess(this.listSuccess)

    }





    listSuccess=(data)=>{
        this.state.dataSource=data.data;
        //this.state.totalRecord=data.totalRecord;
        this.setState({dataSource:this.state.dataSource});
    }

    show=(row)=>{
        this.state.filterForm.memberId.value=row.memberId;
        this.state.filterForm.tradeRecordId.value=row.id;
        this.load();
        this.refs.dialog.show();
    }

    load=()=>{
        let data={};
        for(let key in this.state.filterForm){
            data[key]=this.state.filterForm[key].value;
        }
        DaoFactory.Finance.TradeDetail.data=data;
        DaoFactory.Finance.TradeDetail.load();
    }

    hide=()=>{
        this.refs.dialog.hide();
    }


    handleChange(e){

    }





    getCloumn=()=>{
        return [
            {name:'operatorName',title:'操作人',width:'120px'},
            {name:'operatorDate',title:'操作时间',width:'250px'},
            {name:'isRebate',title:"返利",width:'250px',render:(row)=>{
                if(row.memberTypeCode==="0001"&&row.isRebate===1){
                    return <span>返利金额:{row.rebateAmount},返利比例:{row.rebateRate}%</span>
                }
            }},
            {name:'commissionRate',title:'提成比例(%)',width:'130px'},
            {name:'auditState',title:'是否认证通过',width:'120px',render:(row)=>{
                 if(row.auditState===1){
                     return "通过";
                 }
                if(row.auditState===0){
                    return "待处理";
                }
                if(row.auditState===2){

                    return "未通过";
                }
                }},
            {name:'authorizedDate',title:'认证时间',width:'120px'},


        ];
    }





    render(){

        let item=this.state.dataSource;
        return (
            <Dialog title="详细信息" ref="dialog" {...this.props} >

                    <Card style={{height:300,width:580,position:'relative'}}>
                        <Vbox css="abs-layout">
                            {/*<Form dataSource={this.state.filterForm} ref="form">*/}
                                {/*<FormContent></FormContent>*/}
                            {/*</Form>*/}


                                  <div className="tradedetail">
                                      <div>
                                          <span className="label">收益金额:</span><span>{item.amount}</span><span className="label">收益时间</span><span>{item.createDate}</span>
                                      </div>
                                      <div>
                                          <span className="label">收益来源:</span><span>{item.incomeSource}</span>
                                      </div>
                                      <div>
                                          <span className="label">详细说明:</span><span>{item.detailDescription}</span>
                                      </div>
                                  </div>
                        </Vbox>
                    </Card>
                    <div className="form-command">
                        <Button css="btn-primary"  type="button" margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={this.hide} margin="mg1-right">关闭</Button>
                    </div>

            </Dialog>
        )
    }
}