/**
 * Created by quxiangqian on 2018/6/17.
 */
import {Dialog} from 'trilobita-d/control/Dialog';
import {Form} from 'trilobita-d/form/Form';
import {Button} from 'trilobita-d/control/Button';
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {Radio} from "trilobita-d//form/Radio";
import {DaoFactory} from "config/DaoFactory"
import {CheckBox} from "../comm/CheckBox";
import {toDate} from "../comm/Util"
import {DatesPick} from "../comm/DatesPick"
export class ShipDialog extends React.Component{

    state={
        dataSource:{
            cuzOrderId:{value:"0"},
            shippingId:{value:"1"},
            shipName:{value:'',validate:"null"},
            shipOrder:{value:'',validate:"null"},
            shipStatus:{value:[]},
            shipTime:{value:'',validate:"null"},
        }
    }
    constructor(){
        super();
        DaoFactory.Order.UpdateShipInfo.addSuccess(this.UpdateShipInfoSuccess)
    }
    UpdateShipInfoSuccess=(data)=>{
        if(this.props.saveAfter){
            this.props.saveAfter();
            if(this.hide){
                this.hide();
            }

        }
    }

    show=(row)=>{
        this.refs.dialog.show();
        this.state.dataSource.cuzOrderId.value=row.cuzOrderId;
        if(row["shipTime"]===null||row["shipTime"]==="null"){
            row["shipTime"]="";
        }else{
            row["shipTime"]=toDate(row["shipTime"])
        }

        if(row["shipStatus"]==="1"){
            row["shipStatus"]=[row["shipStatus"]];
        }else{
            row["shipStatus"]=[]
        }
        for(let r in row){
            if(this.state.dataSource[r]){
                this.state.dataSource[r].value=row[r];
            }

        }
        this.setState( {dataSource:this.state.dataSource});
    }

    hide=()=>{
        if(this.refs.dialog){
            this.refs.dialog.hide();
        }

    }


    handleChange(e){

    }


    formContent(props){
        let List=(props)=>{
            return <div onClick={()=>props.comBoxVal(props.val,props.children)}>{props.children}</div>
        }
        let item=props.dataSource;
        // shipName:{value:'',validate:"null"},
        // shipOrder:{value:'',validate:"null"},
        // shipStatus:{value:'',validate:"null"},
        // shipTime:{value:'',validate:"null"},
        return (<table className="form-layout">
            <tr>
                <td>快递名称：</td>
                <td><TextField bind={item} name="shipName"/></td>
            </tr>
            <tr>
                <td>快递订单号：</td>
                <td><TextField bind={item} name="shipOrder"/></td>
            </tr>
            <tr>
                <td>是否发货：</td>
                <td><CheckBox bind={item} name="shipStatus" value="2"></CheckBox></td>
            </tr>
            <tr>
                <td>发货时间：</td>
                <td><DatesPick bind={item} name="shipTime"></DatesPick></td>
            </tr>


        </table>)
    }

    validate=()=>{
        if(this.refs.form.isValiData()){
            let data=this.refs.form.getData();
            if(data.shipStatus.length===0){
                data.shipStatus="1";
            }

           DaoFactory.Order.UpdateShipInfo.data=data;
           DaoFactory.Order.UpdateShipInfo.load();
        }

    }

    render(){

        let FormContent=this.formContent;
        return (
            <Dialog title="发货" ref="dialog" {...this.props} >
                <Form dataSource={this.state.dataSource} ref="form">
                    <FormContent/>
                    <div className="form-command">
                        <Button css="btn-primary"  type="button" onClick={(e)=>{this.validate()}} margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={this.hide} margin="mg1-right">关闭</Button>
                    </div>
                </Form>
            </Dialog>
        )
    }
}