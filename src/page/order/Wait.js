/**
 * Created by quxiangqian on 2018/6/5.
 */
import {DataGrid} from 'trilobita-d/control/DataGrid'
import {Button} from "trilobita-d/control/Button";
import {Panel} from "trilobita-d/control/Panel";
import {Hbox} from "trilobita-d/layout/Hbox";
import {Form} from 'trilobita-d/form/Form';
import {UserInfoDialog} from "page/UserInfoDialog";
import {Card} from "trilobita-d/control/Card";
import {NaviCard} from "trilobita-d/control/NaviCard";
import {Vbox} from "trilobita-d/layout/Vbox";
import {PerPageBar} from "trilobita-d/control/PerPageBar";
import {WindowManager} from "trilobita-d/control/WindowManager";
import {Popover} from "trilobita-d/control/Popover"
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {DaoFactory} from "config/DaoFactory"
import {ImageViewDialog} from "./ImageViewDialog"
import {toDateTime} from "../comm/Util"
import {ShipDialog} from "./ShipDialog"

export default class Wait extends React.Component{

    constructor(){
        super();
        DaoFactory.Order.OrderFind.addSuccess(this.orderFind_Success)
        this.loadOrderlist();
    }
    state={
        totalCount:0,
        dataSource:[

        ],

        filterForm:{
            sn:{value:""},
            start:{value:0},
            length:{value:15},
            payStatus:{value:""},
            shipStatus:{value:"1"},
        }
    }

    orderFind_Success=(data)=>{
        //console.log(data.data);
        this.state.totalCount=data.recordsFiltered;
        this.state.dataSource=data.data;
        this.setState({...this.state.dataSource});
    }

    loadOrderlist=()=>{
        let data={};
        for(let key in this.state.filterForm){
            data[key]=this.state.filterForm[key].value;
        }
        DaoFactory.Order.OrderFind.data={...data}
        DaoFactory.Order.OrderFind.load();
    }

    getCloumn=()=>{
        return [
            {name:'cuzOrderId',title:'订单编号',width:'120px'},
            {name:'name',title:'定制产品',width:'320px',render:(row)=>{
              return(
                  <dl>
                    <dt>[状态：{row.payStatus===2?"已付款":"待支付"}]{row.name}</dt>
                      <dt>订单号：{row.sn}</dt>
                    <dt><label>定购时间：</label><span>{toDateTime(row.orderTime)}</span></dt>
                </dl>
              )

            }},
            {name:'num',title:'价格',width:'150px',render:(row)=>{
                return(
                    <dl>

                        <dt><label>总价格：</label><span>¥{row.totalAmount}</span></dt>
                        <dt><label>购买数量：</label><span>{row.num}</span></dt>
                        <dt><label>单价：</label><span>¥{row.price}</span></dt>

                    </dl>
                )

            }},
            {name:'message',title:'定制元素',width:'420px',render:(data)=>{
            return(
                <div>
                    <dl>
                        <dt><img src={data.thumbnail} onClick={(e)=>{
                            this.imageViewDialog.show({url:data.original,name:data.name})
                        }} style={{width:100,height:75}}></img>
                            <img src={data.compositeImg}
                                 onClick={(e)=>{
                                     this.imageViewDialog.show({url:data.compositeImg,name:data.name})
                                 }}
                                 style={{width:100,height:75,marginLeft:"1rem"}}></img>
                        </dt>
                        <dt><label>祝福语：</label><span>{data.message}</span></dt>
                    </dl>

                </div>
            )
        }},
            {name:'esMemberAddress',title:'送货地址',width:'320px',render:(data)=>{
                let row=data.esMemberAddress;
                if(row==null){
                    return;
                }
                return(
                    <div>
                        <dl>
                            <dt><label>收货人：</label><span>{row.name}</span><a href="javascript:void(0)"
                             onClick={(e)=>{
                                 this.shipDialog.show(data);
                             }}
                            >发货</a></dt>
                            <dt><label>送货地址：</label><span>{row.province}-{row.city}-{row.region}{row.town}{row.addr}</span></dt>
                            <dt><label>联系电话：</label><span>{row.tel}</span></dt>

                        </dl>

                    </div>
                )
            }},

        ];
    }


    componentDidMount(){

    }

    formContent(props){

        let item=props.dataSource;
        const {date, format, mode, inputFormat} = {
            date: "1990-06-05",
            format: "YYYY-MM-DD",
            inputFormat: "DD/MM/YYYY",
            mode: "date"
        };
        return (<table className="form-layout" style={{minWidth:'297px',margin:0,marginBottom:'1rem'}}>
            <tr>
                <td>订单号：</td>
                <td><TextField bind={item} name="sn"/></td>
            </tr>
            <tr>
                <td>支付状态：</td>
                <td>
                    <ComBox readOnly={true} value={item["payStatus"].value} bind={item} name="payStatus">
                        <Option value="">全部</Option>
                        <Option value="2">已付款</Option>
                        <Option value="1">待支付</Option>
                    </ComBox>
                </td>
            </tr>
        </table>)
    }



    render(){
        let FormContent=this.formContent;
        return <Vbox css="abs-layout">
            <Popover title="筛选" ref={(c)=>{this.filterForm=c}} body={
                <Form dataSource={this.state.filterForm} style={{margin:0}} >
                    <FormContent/>
                    <div className="form-command">
                        <Button css="btn-primary"   type="button"  onClick={()=>{
                            this.loadOrderlist();
                            this.filterForm.hide();
                        }}  margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={()=>this.filterForm.hide()} margin="mg1-right">关闭</Button>
                    </div>
                </Form>
            } pos="left-top"><i class="fas fa-filter"
                                                 style={{fontSize:16,color:"#666",right:'1rem',top:'7rem',borderRadius:'0.5rem', border:"solid 1px #ccc",padding:'0.5rem', position:"fixed",cursor:"pointer"}}></i>
            </Popover>
            <ImageViewDialog ref={(dialog)=>this.imageViewDialog=dialog}></ImageViewDialog>
            <ShipDialog ref={(dialog)=>this.shipDialog=dialog} saveAfter={(e)=>{
                this.loadOrderlist();
            }}></ShipDialog>
            <WindowManager ref="wm"/>
            <UserInfoDialog ref="dialog"></UserInfoDialog>
            <NaviCard title="待处理" dataSource={["首页","订单管理","待处理"]} description="根据条件查询订单情况"></NaviCard>
            <Card css="mg4" style={{height:"100%"}}>
                <Vbox css="abs-layout">

                     <DataGrid ref="dg" cloumn={this.getCloumn()} dataSource={this.state.dataSource}></DataGrid>
                     <PerPageBar pageSize={this.state.filterForm.length.value}
                                 total={this.state.totalCount}
                                 goTo={(index,start,end,pagesize)=>{
                                       this.state.filterForm.start.value=start;
                                       this.state.filterForm["length"].value=pagesize;
                                       this.loadOrderlist();
                                     }
                                 }
                                 style={{minHeight:"2.4rem",marginTop:'1rem'}}/>
                </Vbox>


            </Card>
        </Vbox>
    }
}