/**
 * Created by quxiangqian on 2018/6/17.
 */
import {Dialog} from 'trilobita-d/control/Dialog';
import {Form} from 'trilobita-d/form/Form';
import {Button} from 'trilobita-d/control/Button';
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {Radio} from "trilobita-d//form/Radio";
import {DaoFactory} from "config/DaoFactory"


export class ImageViewDialog extends React.Component{

    state={
        url:"",
        name:""
    }

    constructor(){
        super();
    }


    show=(row)=>{
        this.refs.dialog.show();
        this.state.url=row.url;
        this.state.name=row.name;
        this.setState( {...this.state});
    }

    hide=()=>{
        this.refs.dialog.hide();
    }


    handleChange(e){

    }







    render(){

        let FormContent=this.formContent;
        return (
            <Dialog title="图片查看" ref="dialog" {...this.props} >
                    <div width={700} height={500} style={{width:700,height:500,overflow:"auto"}}>
                    <img src={this.state.url}></img>
                    </div>
                    <div className="form-command">
                        <a href={this.state.url} target="_blank" download={this.state.name} style={{border:0}}>
                            <Button css="btn-default"  type="button" margin="mg1-right">下载</Button>
                        </a>
                        <Button css="btn-default"  type="button" onClick={this.hide} margin="mg1-right">关闭</Button>
                    </div>

            </Dialog>
        )
    }
}