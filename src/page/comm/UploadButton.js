/**
 * Created by quxiangqian on 2018/7/20.
 */
export class UploadButton extends React.Component{
    //mjapi.conpanda.cn
    //https://mingjiu-api.conpanda.cn/fileserver/uploadImage
    init=(c)=>{
        let url='https://mjapi.conpanda.cn/fileserver/uploadImage'
        let ext={
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/*'
           };
        if(this.props.type==="video"){
            url="https://mjapi.conpanda.cn/fileserver/uploadVideo"
            let _extensions ='3gp,mp4,rmvb,mov,avi,m4v';
            let _mimeTypes ='video/*,audio/*,application/*';
            ext={
                title: 'video',
                extensions:_extensions,
                mimeTypes: _mimeTypes
            }
        }
        if(!c){
            return;
        }
        var uploader = WebUploader.create({
            pick: {id:c,label:this.props.children},
            server:url,
            auto: true,
            fileVal:"file",
            method:'POST',
            chunked:false,
            fileNumLimit : 1,
            duplicate:true,
            accept:ext,
        });
        uploader.reset();
        //uploader.uploadAccept.
        uploader.on("uploadAccept",this.uploadAccept);
        uploader.on("uploadComplete",(file)=>{
            if(this.props.proccess){
                this.props.proccess(false,100);
            }
            uploader.reset();
        });
        uploader.on("error",(s)=>{

            uploader.reset();
        });
        uploader.on("uploadStart",(s)=>{
            if(this.props.proccess){
                this.props.proccess(true,0);
            }
        });
        uploader.on("uploadProgress",(f,n)=>{
            if(this.props.proccess){
                this.props.proccess(true,parseInt(n*100));
            }
        });



        // uploader.on('uploadBeforeSend', function(obj, data, headers) {
        //     $.extend(headers, {
        //         "Origin": "*",
        //         "Access-Control-Request-Method": "POST"
        //     });
        // });

    }
    uploadAccept=(obj,ref)=>{
        console.log(ref,this.props.success)
      if(this.props.success){
          this.props.success(ref.remoteUrl);
      }
    }
    render(){
        return <div className="uploadbutton" ref={this.init} >{this.props.children}</div>
    }
}