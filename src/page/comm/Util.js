/**
 * Created by quxiangqian on 2018/7/22.
 */
export function toDateTime(i){
    let d=new Date(i);

    return d.getFullYear()+"-"+toDayString(d.getMonth()+1)+"-"+toDayString(d.getDate())+" "+toDayString(d.getHours())+":"+toDayString(d.getMinutes())+":"+toDayString(d.getSeconds())
}

export function toDate(i){
    let d=new Date(i);

    return d.getFullYear()+"-"+toDayString(d.getMonth()+1)+"-"+toDayString(d.getDate())
}

function toDayString(s) {
    if(s<10){
        return "0"+s;
    }
    return s;
}