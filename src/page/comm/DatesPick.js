/**
 * Created by quxiangqian on 2018/7/19.
 */


export class DatesPick extends React.Component {
    state={
        value:"",
        error:false,
        message:""
    }
    onChange=(e,item)=>{
        item.value=e.target.value;
        this.setState({value:e.target.value});
    }

    showError=(message)=>{
        this.state.error=true;

        this.setState({error:true,message:message});
    }

    default=()=>{
        this.setState({error:false});
    }

    inputInit=(e)=>{
        $.datetimepicker.setLocale('ch');
        $(e).datetimepicker({format:"Y-m-d",timepicker:false});
    }
    render(){
        let item= this.props.bind[this.props.name];

        item.showError=this.showError;
        item.default=this.default;

        if(item.value){
            this.state.value=item.value;
        }
        let style={}
        let tem=<input type="text" {...this.props}   name={this.props.name} value={this.state.value} ref={this.inputInit} onChange={(e)=>this.onChange(e,item)} onBlur={(e)=>this.onChange(e,item)} className="form-control" autocomplete="off" />

        if(this.state.error===true){
            style.borderColor="red";
            tem=<ToolTip text={this.state.message} css="error" pos="right"><input  style={style} type="text" {...this.props} ref={this.inputInit} onBlur={(e)=>this.onChange(e,item)} name={this.props.name} value={this.state.value} autocomplete="off"  onChange={(e)=>this.onChange(e,item)} className="form-control"/></ToolTip>
        }

        return tem;
    }
}