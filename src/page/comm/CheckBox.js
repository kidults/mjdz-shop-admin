/**
 * Created by quxiangqian on 2018/7/7.
 */

export class CheckBox extends React.Component {
    state={
        item:false,
        error:false,
        message:"",
    }
    onChange=(e,item)=>{
        //item.value=e.target.value;
        //this.props.checked=!this.props.checked;
        //let a=[];
        if(!e.target.checked){
            this.state.item.value=this.state.item.value.filter((it)=>it!==e.target.value)
        }else{
            this.state.item.value.push(e.target.value);
            //a=this.state.item.value;
        }
        //this.state.item.value=a;

        this.setState({item:this.state.item});
    }

    showError=(message)=>{
        //this.setState({error:true,message:message});
    }

    default=()=>{
        //this.setState({error:false});
    }

    render(){
        if(!this.state.item){
            this.state.item= this.props.bind[this.props.name];
        }


        if(!this.state.item.showError){
            this.state.item.showError=this.showError;
        }
        if(!this.state.item.default){
            this.state.item.default=this.default;
        }

        let tem=(<label style={{marginRight:'1rem'}}>
            <input style={{marginRight:'0.5rem'}}
                   onClick={(e)=>{this.onChange(e,this.state.item)}}
                   type="checkbox"
                   ref={(da)=>{
                        if(da){
                            if(this.state.item.value.includes(this.props.value)){
                                da.checked=true;
                            }else{
                                da.checked=false;
                            }
                        }
        }}  name={this.props.name}  value={this.props.value} ></input>{this.props.text}</label>)

        return tem;
    }
}