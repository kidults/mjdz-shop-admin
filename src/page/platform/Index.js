/**
 * Created by quxiangqian on 2018/6/5.
 */

import {Calendar} from "trilobita-d/form/Calendar";
import {NaviCard} from "trilobita-d/control/NaviCard"
import {Vbox} from "trilobita-d/layout/Vbox"
import {Card} from "trilobita-d/control/Card"
import {Button} from "trilobita-d/control/Button"
import {Form} from "trilobita-d/form/Form"
import {TextField} from "trilobita-d/form/TextField";
import {DaoFactory} from "../../config/DaoFactory"
import {WindowManager} from "trilobita-d/control/WindowManager";
import {UploadButton} from "../comm/UploadButton";


export default class Index extends React.Component{

    state={
       url:"",
        dataSource:{
           logoImg:{value:""}
       }
    }


    constructor(){
        super();

        //this.load();
        DaoFactory.QrCode.SaveLogo.addSuccess(this.updateSuccess);
        this.state.url=DaoFactory.QrCode.PlatForm+"?"+parseInt(new Date().getTime()/1000)
    }

    updateSuccess=(data)=>{
        if(data.code===200){
            this.setState({url:DaoFactory.QrCode.PlatForm+"?"+parseInt(new Date().getTime()/1000)});
        }
    }





    proccess=(b,n)=>{
        this.state.upload=b;
        this.state.process=n;
        this.setState({...this.state});
    }

    formContent=(props)=>{

        let item=props.dataSource;

        return (<div style={{textAlign:"center"}}>

            <img src={this.state.url} style={{width:400,height:400}}></img>
            <div>
                <UploadButton success={(s)=>{
                    //item["logoImg"].value=s;
                    DaoFactory.QrCode.SaveLogo.data={logoImg:s}
                    DaoFactory.QrCode.SaveLogo.load();

                }} proccess={this.proccess} >定制Logo</UploadButton>
            </div>

        </div>);
    }

    render(){
        //this.state.url=DaoFactory.QrCode.PlatForm+"?"+parseInt(new Date().getTime()/1000);
        let FormContent=this.formContent;
        return (<Vbox css="abs-layout">
            <WindowManager ref={(wm)=>this.win=wm}/>
            <NaviCard title="平台二维码" dataSource={["首页","系统管理","平台二维码"]} description="扫描当前二维码，成为一级经销商"></NaviCard>
            <Card css="mg4" style={{height:"100%"}}>
                <Vbox css="abs-layout">

                    <Form dataSource={this.state.dataSource} ref="form">
                        <FormContent/>
                        <div>
                            {this.state.upload ?
                                <div style={{background:"#c3c3c3",margin:"1rem"}}>
                                    <div style={{background:"#81cb92",width:this.state.process+"%",display:"block",textAlign:"center"}}>{this.state.process}%</div>
                                </div>
                                :""}
                        </div>

                    </Form>
                </Vbox>
            </Card>
        </Vbox>)
    }
}