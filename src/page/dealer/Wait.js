/**
 * Created by quxiangqian on 2018/6/5.
 */
import {DataGrid} from 'trilobita-d/control/DataGrid'
import {Button} from "trilobita-d/control/Button";
import {Panel} from "trilobita-d/control/Panel";
import {Hbox} from "trilobita-d/layout/Hbox";
import {Form} from 'trilobita-d/form/Form';
import {AuditDialog} from "./AuditDialog";
import {Card} from "trilobita-d/control/Card";
import {NaviCard} from "trilobita-d/control/NaviCard";
import {Vbox} from "trilobita-d/layout/Vbox";
import {PerPageBar} from "trilobita-d/control/PerPageBar";
import {WindowManager} from "trilobita-d/control/WindowManager";
import {Popover} from "trilobita-d/control/Popover"
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {DaoFactory} from "config/DaoFactory"
import {Dialog} from 'trilobita-d/control/Dialog';
import {ToolTip} from "trilobita-d/control/ToolTip"


export default class Wait extends React.Component{

    constructor(){
        super();
        DaoFactory.Dealer.AuditNotList.addSuccess(this.List_Success)
        this.loadList();
    }
    state={
        totalRecord:0,
        dataSource:[

        ],
        idCardDetail:{
            name:'',
            fphto:'',
            bphto:'',
        },
        filterForm:{
            keyword:{value:""},
            pageNumber:{value:1},
            pageSize:{value:15},
        }
    }

    List_Success=(data)=>{
        //console.log(data.data);
        this.state.totalRecord=data.totalRecord;
        this.state.dataSource=data.data;
        this.setState({...this.state.dataSource});
    }

    loadList=()=>{
        let data={};
        for(let key in this.state.filterForm){
            data[key]=this.state.filterForm[key].value;
        }

        DaoFactory.Dealer.AuditNotList.data={...data}
        DaoFactory.Dealer.AuditNotList.load();
    }

    getCloumn=()=>{
        return [
            {name:'memberId',title:'编号',width:'120px'},

            {name:'uname',title:'用户名',width:'150px'},
            {name:'tel',title:'电话',width:'150px'},

            {name:'idcardNo',title:'身份证号码',width:'250px'},
            {name:'name',title:'身份证姓名',width:'150px'},
            {name:'memberTypeCode',title:'经销商类型',width:'150px',render:(row)=>{
                if(row.memberTypeCode==="0000"){
                    return "平台"
                }
                if(row.memberTypeCode==="0001"){
                    return "城市合伙人"
                }
                if(row.memberTypeCode==="0002"){
                    return "分销商"
                }
            }},

            {name:'createDate',title:'申请时间',width:'250px'},
            {name:'commissionRate',title:'提成比例(%)',width:'250px'},
            {name:'isRebate',title:"返利",width:'250px',render:(row)=>{
                if(row.memberTypeCode==="0001"&&row.isRebate===1){
                    return <span>返利金额:{row.rebateAmount},返利比例:{row.rebateRate}%</span>
                }
            }},
            {name:'status',title:'是否实名',width:'220px',render:(row)=>{
                let s="";
                if(row.status===1){
                    s="待审核"
                }
                if(row.status===2){
                    s="异常"
                }
                if(row.status===3){
                    s="通过"
                }
                return<span>[{s}]</span>
            }},
            {name:'work',title:'操作',width:'250px',render:(row)=>{
                if(row.memberTypeCode==="0000"){
                    return ""
                }
                return <a href="javascript:void(0)" onClick={(e)=>this.auditDialog.show(row)}>审核</a>
            }},



        ];
    }


    componentDidMount(){

    }

    formContent(props){

        let item=props.dataSource;
        return (<table className="form-layout" style={{minWidth:'297px',margin:0,marginBottom:'1rem'}}>
            <tr>
                <td>关键词：<ToolTip text="请输入手机号码、用户名、姓名、身份证的任意关键词"><i class="fas fa-info-circle" style={{fontSize:"1rem"}}></i></ToolTip></td>
                <td><TextField bind={item} name="keyword"  ></TextField></td>
            </tr>
        </table>)
    }



    render(){
        let FormContent=this.formContent;
        return <Vbox css="abs-layout">
            <Popover title="筛选" ref={(c)=>{this.filterForm=c}} body={
                <Form dataSource={this.state.filterForm} style={{margin:0}} >
                    <FormContent/>
                    <div className="form-command">
                        <Button css="btn-primary"   type="button"  onClick={()=>{
                            this.loadList();
                            this.filterForm.hide();
                        }}  margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={()=>this.filterForm.hide()} margin="mg1-right">关闭</Button>
                    </div>
                </Form>
            } pos="left-top">
                <i class="fas fa-filter" style={{fontSize:16,color:"#666",right:'1rem',top:'7rem',borderRadius:'0.5rem', border:"solid 1px #ccc",padding:'0.5rem', position:"fixed",cursor:"pointer"}}></i>
            </Popover>
            <AuditDialog ref={(dialog)=>this.auditDialog=dialog} saveAfter={()=>{
                this.loadList();
            }}/>
            <WindowManager ref="wm"/>



            <NaviCard title="待审核" dataSource={["首页","经销商管理","待审核"]} description="根据条件查询经销商情况"></NaviCard>

            <Card css="mg4" style={{height:"100%"}}>
                <Vbox css="abs-layout">

                    <DataGrid ref="dg" cloumn={this.getCloumn()} dataSource={this.state.dataSource}></DataGrid>
                    <PerPageBar pageSize={this.state.filterForm.pageSize.value}
                                total={this.state.totalRecord}
                                goTo={(index,start,end,pagesize)=>{
                                    this.state.filterForm.pageNumber.value=index;
                                    this.state.filterForm.pageSize.value=pagesize;
                                    this.loadList();
                                }
                                }
                                style={{minHeight:"2.4rem",marginTop:'1rem'}}/>
                </Vbox>


            </Card>
        </Vbox>
    }
}