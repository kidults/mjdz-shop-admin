/**
 * Created by quxiangqian on 2018/6/17.
 */
import {Dialog} from 'trilobita-d/control/Dialog';
import {Form} from 'trilobita-d/form/Form';
import {Button} from 'trilobita-d/control/Button';
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {Radio} from "../comm/Radio";
import {CheckBox} from "../comm/CheckBox";
import {DaoFactory} from "config/DaoFactory"


export class AuditDialog extends React.Component{

    state={
        systemRate:0,
        commissionRate1:0,
        commissionRate2:0,
        dataSource:{
            memberId:{value:""},
            status:{value:"3"},
            isAuthorized:{value:[]},
            auditState:{value:""},
            isRebate:{value:"0"},
            operatorId:{value:''},
            operatorName:{value:''},
            rebateRate:{value:"",func:(item)=>{

                if(this.state.dataSource.isRebate.value==="1"&&item.value===""){

                    if(item.showError){
                        item.showError("不能为空");
                    }
                    //alert("标示异常，备注被能为空")
                    return false;
                }else{
                    if(item.default){
                        item.default();
                    }

                    return true;
                }
            }},
            rebateAmount:{value:"",func:(item)=>{

                if(this.state.dataSource.isRebate.value==="1"&&item.value===""){
                    if(item.showError){
                        item.showError("不能为空");
                    }
                    return false;
                }else{
                    if(item.default){
                        item.default();
                    }
                    return true;
                }
            }},
            memberTypeCode:{value:""},
            commissionRate:{value:""},
            authorizedRemark:{value:"",func:(item)=>{

                if(this.state.dataSource.auditState.value==="2"&&item.value===""){
                    item.showError("未通过，备注被能为空");
                    //alert("标示异常，备注被能为空")
                    return false;
                }else{
                    item.default();
                    return true;
                }
            }}
        }
    }
    constructor(){
        super();
        DaoFactory.Dealer.Authorize.clear();
        DaoFactory.Dealer.Authorize.addSuccess(this.saveSuccess)
        //DaoFactory.SystemConfig.One.addSuccess(this.oneSuccess)
        //DaoFactory.SystemConfig.One.data={};
        //DaoFactory.SystemConfig.One.load();

    }
    oneSuccess=(data)=>{
        let row=data.data;
        this.state.commissionRate1=row.commissionRate1;
        this.state.commissionRate2=row.commissionRate2;


    }
    saveSuccess=(data)=>{
        if(this.props.saveAfter){
            this.props.saveAfter();
            if(this.hide){
                this.hide();
            }

        }
    }

    show=(row)=>{
        if(this.dialog){
            this.dialog.show();
        }
        for(let r in row){
            row[r]=row[r]+"";
        }
        if(row["isAuthorized"]==="1"){
            row["isAuthorized"]=[row["isAuthorized"]];
        }else{
            row["isAuthorized"]=[]
        }

        for(let r in row){
            if(this.state.dataSource[r]){
                this.state.dataSource[r].value=row[r];
            }
        }
        console.log(this.state.dataSource);
        let userinfo=JSON.parse(sessionStorage.getItem("userInfo"));
        this.state.dataSource.operatorId.value=userinfo.userid;
        this.state.dataSource.operatorName.value=userinfo.username;
        this.setState( {dataSource:this.state.dataSource});

    }

    hide=()=>{
        if(this.dialog){
            this.dialog.hide();
        }
        console.log(this.dialog)

    }


    handleChange(e){

    }


    formContent=(props)=>{

        let item=props.dataSource;
        let status="通过";
        if(this.state.dataSource.status.value===1){
            status="未通过"
        }else if(this.state.dataSource.status.value===2)
        {
            status="异常"
        }else {
            status="通过"
        }
        let memberType="";

        if(this.state.dataSource.memberTypeCode.value==="0000"){
            memberType="平台"
        }else if(this.state.dataSource.memberTypeCode.value==="0001"){
            memberType="城市合伙人"
            this.state.systemRate=this.state.commissionRate1;

        }else if(this.state.dataSource.memberTypeCode.value==="0002"){
            memberType="分销商"
            this.state.systemRate=this.state.commissionRate2;
        }
        return (<table className="form-layout">
            <tr>
                <td>实名认证：</td>
                <td>{status}</td>
            </tr>
            <tr>
                <td>是否签订合同：</td>
                <td><CheckBox bind={item} name="isAuthorized" value="1"></CheckBox></td>
            </tr>
            <tr>
                <td>授权级别：</td>
                <td>{memberType}</td>
            </tr>
            <tr>
                <td>设置提成比例：</td>
                <td  style={{display:"flex",justifyContent:'flex-start'}}><TextField bind={item} name="commissionRate"/></td>
            </tr>
            {this.state.dataSource.memberTypeCode.value === "0001" ?
                <tr>
                    <td>是否返利：</td>
                    <td><Radio bind={item} name="isRebate" value="0" text="不返利" onClick={(e) => {
                        this.setState({...this.state})
                    }}></Radio><Radio bind={item} name="isRebate" value="1" text="返利"
                                      onClick={(e) => {

                                          this.setState({...this.state})
                                      }}
                    ></Radio></td>
                </tr>:""
            }
            {
                item.isRebate.value==="1"? <tr>
                    <td>返利总金额：</td>
                    <td><TextField bind={item} name="rebateAmount"/></td>
                </tr>:""
            }
            {
                item.isRebate.value==="1"? <tr>
                    <td>返利比例：</td>
                    <td><TextField bind={item} name="rebateRate"/></td>
                </tr>:""
             }
            <tr>
                <td>是否给予通过：</td>
                <td><Radio bind={item} name="auditState" value="0" text="待处理"></Radio><Radio bind={item} name="auditState" value="2" text="未通过"></Radio><Radio bind={item} name="auditState" value="1" text="通过"></Radio></td>
            </tr>
            <tr>
                <td>备注：</td>
                <td><TextField bind={item} name="authorizedRemark"/></td>
            </tr>


        </table>)
    }

    validate=()=>{
        if(this.refs.form.isValiData()){

            let data=this.refs.form.getData();
            if(data.isAuthorized.length===0){
                data.isAuthorized="0";
            }
            DaoFactory.Dealer.Authorize.data=data;
            DaoFactory.Dealer.Authorize.load();
        }

    }

    render(){

        let FormContent=this.formContent;
        return (
            <Dialog title="经销商审核" ref={(dialog)=>this.dialog=dialog} {...this.props} >
                <Form dataSource={this.state.dataSource} ref="form">
                    <FormContent/>
                    <div className="form-command">
                        <Button css="btn-primary"  type="button" onClick={(e)=>{this.validate()}} margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={this.hide} margin="mg1-right">关闭</Button>
                    </div>
                </Form>
            </Dialog>
        )
    }
}