/**
 * Created by quxiangqian on 2018/6/17.
 */
import {Dialog} from 'trilobita-d/control/Dialog';
import {Form} from 'trilobita-d/form/Form';
import {Button} from 'trilobita-d/control/Button';
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {Radio} from "trilobita-d/form/Radio";
import {DaoFactory} from "config/DaoFactory"
import {DataGrid} from 'trilobita-d/control/DataGrid'
import {Card} from "trilobita-d/control/Card";
import {Vbox} from "trilobita-d/layout/Vbox";
import {PerPageBar} from "trilobita-d/control/PerPageBar";
export class AuditDetailDialog extends React.Component{

    state={
        totalRecord:0,
        dataSource:[

        ],

        filterForm:{
            memberId:{value:""},
            pageNumber:{value:1},
            pageSize:{value:15},
        }

    }
    constructor(){
        super();
        DaoFactory.Dealer.AuditHistory.addSuccess(this.listSuccess)

    }





    listSuccess=(data)=>{
        this.state.dataSource=data.data;
        this.state.totalRecord=data.totalRecord;
        this.setState({dataSource:this.state.dataSource});
    }

    show=(row)=>{
        this.state.filterForm.memberId.value=row.memberId;
        this.load();
        this.refs.dialog.show();
    }

    load=()=>{
        let data={};
        for(let key in this.state.filterForm){
            data[key]=this.state.filterForm[key].value;
        }
        DaoFactory.Dealer.AuditHistory.data=data;
        DaoFactory.Dealer.AuditHistory.load();
    }

    hide=()=>{
        this.refs.dialog.hide();
    }


    handleChange(e){

    }





    getCloumn=()=>{
        return [
            {name:'operatorName',title:'操作人',width:'120px'},
            {name:'operatorDate',title:'操作时间',width:'250px'},
            {name:'isRebate',title:"返利",width:'250px',render:(row)=>{
                if(row.memberTypeCode==="0001"&&row.isRebate===1){
                    return <span>返利金额:{row.rebateAmount},返利比例:{row.rebateRate}%</span>
                }
            }},
            {name:'commissionRate',title:'提成比例(%)',width:'130px'},
            {name:'auditState',title:'是否认证通过',width:'120px',render:(row)=>{
                 if(row.auditState===1){
                     return "通过";
                 }
                if(row.auditState===0){
                    return "待处理";
                }
                if(row.auditState===2){

                    return "未通过";
                }
                }},
            {name:'authorizedDate',title:'认证时间',width:'120px'},


        ];
    }





    render(){


        return (
            <Dialog title="审核历史记录" ref="dialog" {...this.props} >

                    <Card style={{height:300,width:780,position:'relative'}}>
                        <Vbox css="abs-layout">
                            {/*<Form dataSource={this.state.filterForm} ref="form">*/}
                                {/*<FormContent></FormContent>*/}
                            {/*</Form>*/}
                            <DataGrid ref="dg" cloumn={this.getCloumn()} dataSource={this.state.dataSource}></DataGrid>
                            <PerPageBar pageSize={this.state.filterForm.pageSize.value}
                                        total={this.state.totalRecord}
                                        goTo={(index,start,end,pagesize)=>{
                                            this.state.filterForm.pageNumber.value=index;
                                            this.state.filterForm.pageSize.value=pagesize;
                                            this.load();
                                        }
                                        }
                                        style={{minHeight:"2.4rem",marginTop:'1rem'}}/>
                        </Vbox>
                    </Card>
                    <div className="form-command">
                        <Button css="btn-primary"  type="button" margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={this.hide} margin="mg1-right">关闭</Button>
                    </div>

            </Dialog>
        )
    }
}