/**
 * Created by quxiangqian on 2018/6/17.
 */
import {Dialog} from 'trilobita-d/control/Dialog';
import {Form} from 'trilobita-d/form/Form';
import {Button} from 'trilobita-d/control/Button';
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";


export class UserInfoDialog extends React.Component{
    state={
        dataSource:{
            username:{value:"quxiangqian",validate:"null"},
            password:{value:"123456",validate:"null"},
            combox:{value:"2"}
        }

    }
    show=()=>{
        this.refs.dialog.show();
        this.setState( {dataSource:{
            username:{value:"quxiangqian",validate:"null"},
            password:{value:"123456",validate:"null"},
            combox:{value:"2"}
        }});
    }
    hide=()=>{
        this.refs.dialog.hide();
    }
    handleChange(e){

    }
    formContent(props){
        let List=(props)=>{
            return <div onClick={()=>props.comBoxVal(props.val,props.children)}>{props.children}</div>
        }
        let item=props.dataSource;
        const {date, format, mode, inputFormat} = {
            date: "1990-06-05",
            format: "YYYY-MM-DD",
            inputFormat: "DD/MM/YYYY",
            mode: "date"
        };
        return (<table className="form-layout">
            <tr>
                <td>用户名：</td>
                <td><TextField bind={item} name="username"/></td>
            </tr>
            <tr>
                <td>密码：</td>
                <td><TextField bind={item} name="password"/></td>
            </tr>
            <tr>
                <td>生日：</td>
                <td><DateTimeField/></td>
            </tr>
            <tr>
                <td>下拉框：</td>
                <td>
                    <ComBox readOnly={true} bind={item} value="2" name="combox">
                        <Option value="">全部</Option>
                        <Option value="1">中国</Option>
                        <Option value="2">日本</Option>
                        <Option value="3">美国</Option>
                        <Option value="4">英国</Option>
                    </ComBox>
                </td>
            </tr>
        </table>)
    }

    validate=()=>{
        this.refs.form.isValiData();
    }

    render(){

        let FormContent=this.formContent;
        return (
            <Dialog title="模式窗口" ref="dialog" {...this.props} >
                <Form dataSource={this.state.dataSource} ref="form">
                    <FormContent/>
                    <div className="form-command">
                        <Button css="btn-primary"  type="button" onClick={(e)=>{this.validate()}} margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={this.hide} margin="mg1-right">关闭</Button>
                    </div>
                </Form>
            </Dialog>
        )
    }
}