/**
 * Created by quxiangqian on 2018/6/5.
 */
import {DataGrid} from 'trilobita-d/control/DataGrid'
import {Button} from "trilobita-d/control/Button";
import {Panel} from "trilobita-d/control/Panel";
import {Hbox} from "trilobita-d/layout/Hbox";
import {Form} from 'trilobita-d/form/Form';
import {Card} from "trilobita-d/control/Card";
import {NaviCard} from "trilobita-d/control/NaviCard";
import {Vbox} from "trilobita-d/layout/Vbox";
import {PerPageBar} from "trilobita-d/control/PerPageBar";
import {WindowManager} from "trilobita-d/control/WindowManager";
import {Popover} from "trilobita-d/control/Popover"
import {ToolTip} from "trilobita-d/control/ToolTip"


import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {DaoFactory} from "config/DaoFactory"
import {Dialog} from 'trilobita-d/control/Dialog';


export default class History extends React.Component{

    constructor(){
        super();
        DaoFactory.SystemConfig.His.addSuccess(this.List_Success)
        this.loadList();
    }
    state={
        totalRecord:0,
        dataSource:[

        ],
        idCardDetail:{
            name:'',
            fphto:'',
            bphto:'',
        },
        filterForm:{
            keyword:{value:""},
            pageNumber:{value:1},
            pageSize:{value:15},
        }
    }

    List_Success=(data)=>{
        //console.log(data.data);
        this.state.totalRecord=data.totalRecord;
        this.state.dataSource=data.data;
        this.setState({...this.state.dataSource});
    }

    loadList=()=>{
        let data={};
        for(let key in this.state.filterForm){
            data[key]=this.state.filterForm[key].value;
        }

        DaoFactory.SystemConfig.His.data=data;
        DaoFactory.SystemConfig.His.load();
    }

    getCloumn=()=>{
        return [
            {name:'operatorName',title:'操作人',width:'120px'},

            {name:'perOrderAmount',title:'返利基本线',width:'150px'},
            {name:'perOrderRebate',title:'返利金额',width:'150px'},

            {name:'commissionRate1',title:'一级提成（%）',width:'250px'},
            {name:'commissionRate2',title:'二级提成（%）',width:'150px'},

            {name:'operatorDate',title:'修改时间',width:'250px'},




        ];
    }


    componentDidMount(){

    }

    formContent(props){

        let item=props.dataSource;
        return (<table className="form-layout" style={{minWidth:'297px',margin:0,marginBottom:'1rem'}}>
            <tr>
                <td>关键词：<ToolTip text="请输入手机号码、用户名、姓名、身份证的任意关键词"><i class="fas fa-info-circle" style={{fontSize:"1rem"}}></i></ToolTip></td>
                <td><TextField bind={item} name="keyword"  ></TextField></td>
            </tr>
        </table>)
    }



    render(){
        let FormContent=this.formContent;
        return <Vbox css="abs-layout">
            <Popover title="筛选" ref={(c)=>{this.filterForm=c}} body={
                <Form dataSource={this.state.filterForm} style={{margin:0}} >
                    <FormContent/>
                    <div className="form-command">
                        <Button css="btn-primary"   type="button"  onClick={()=>{
                            this.loadList();
                            this.filterForm.hide();
                        }}  margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={()=>this.filterForm.hide()} margin="mg1-right">关闭</Button>
                    </div>
                </Form>
            } pos="left-top">
                <i class="fas fa-filter" style={{fontSize:16,color:"#666",right:'1rem',top:'7rem',borderRadius:'0.5rem', border:"solid 1px #ccc",padding:'0.5rem', position:"fixed",cursor:"pointer"}}></i>
            </Popover>

            <WindowManager ref="wm"/>



            <NaviCard title="系统设置历史" dataSource={["首页","系统管理","系统设置历史"]} description="根据条件系统设置的历史记录情况"></NaviCard>

            <Card css="mg4" style={{height:"100%"}}>
                <Vbox css="abs-layout">

                     <DataGrid ref="dg" cloumn={this.getCloumn()} dataSource={this.state.dataSource}></DataGrid>
                     <PerPageBar pageSize={this.state.filterForm.pageSize.value}
                                 total={this.state.totalRecord}
                                 goTo={(index,start,end,pagesize)=>{
                                       this.state.filterForm.pageNumber.value=index;
                                       this.state.filterForm.pageSize.value=pagesize;
                                       this.loadList();
                                     }
                                 }
                                 style={{minHeight:"2.4rem",marginTop:'1rem'}}/>
                </Vbox>


            </Card>
        </Vbox>
    }
}