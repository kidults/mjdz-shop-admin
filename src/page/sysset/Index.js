/**
 * Created by quxiangqian on 2018/6/5.
 */

import {Calendar} from "trilobita-d/form/Calendar";
import {NaviCard} from "trilobita-d/control/NaviCard"
import {Vbox} from "trilobita-d/layout/Vbox"
import {Card} from "trilobita-d/control/Card"
import {Button} from "trilobita-d/control/Button"
import {Form} from "trilobita-d/form/Form"
import {TextField} from "trilobita-d/form/TextField";
import {DaoFactory} from "../../config/DaoFactory"
import {WindowManager} from "trilobita-d/control/WindowManager";


export default class Index extends React.Component{

    state={
        dataSource: {
            configId: {value: ""},
            operatorId:{value:''},
            operatorName:{value:''},
            commissionRate1: {value: "",func:(item)=>{return this.valNum(item)}},
            commissionRate2: {value: "",func:(item)=>{return this.valNum(item)}},
            perOrderAmount: {value: "",func:(item)=>{return this.valNum(item)}},
            perOrderRebate:  {value: "",func:(item)=>{return this.valNum(item)}},
        }
    }

    valNum=(item)=>{
        if(isNaN(Number(item.value)))
        {
            item.showError("只能是数字");
            return false;
        }else if(item.value===""){
            item.showError("不能为空");
            return false;
        }else {
            item.default();
        }
        return true;
    }

    constructor(){
        super();
        DaoFactory.SystemConfig.One.addSuccess(this.oneSuccess);
        DaoFactory.SystemConfig.Update.addSuccess(this.updateSuccess)
        this.load();
    }

    updateSuccess=(data)=>{
        if(data.code===200){
            this.win.alert("保存成功");
            this.load();
        }
    }

    load=()=>{

        DaoFactory.SystemConfig.One.data={};
        DaoFactory.SystemConfig.One.load();
    }

    oneSuccess=(data)=>{
        let item=data.data;
        for(let key in this.state.dataSource){
            this.state.dataSource[key].value=item[key];
        }
        let userinfo=JSON.parse(sessionStorage.getItem("userInfo"));
        this.state.dataSource.operatorId.value=userinfo.userid;
        this.state.dataSource.operatorName.value=userinfo.username;
        //console.log(this.state.dataSource)
        this.setState({dataSource:this.state.dataSource});
    }

    save=()=>{

        if(this.refs.form.isValiData()){
            this.win.confirm("你确认要保存当前数据？",(e)=>{
                // let item={};
                // for(let key in this.state.dataSource){
                //     item[key]=this.state.dataSource[key].value;
                // }
                DaoFactory.SystemConfig.Update.data=this.refs.form.getData();
                DaoFactory.SystemConfig.Update.load();
            })

        }
    }

    formContent(props){

        let item=props.dataSource;

        return (<table className="form-layout" style={{width:'100%'}}>
            <tr>
                <td cols={2}><h3>提成比例</h3></td>
            </tr>
            <tr>
                <td style={{width:120}}>一级：</td>
                <td style={{display:"flex",justifyContent:'flex-start'}}><TextField bind={item} name="commissionRate1"/><span style={{lineHeight:"1.5rem",padding:".5rem"}}>%</span></td>
            </tr>
            <tr>
                <td>二级：</td>
                <td style={{display:"flex",justifyContent:'flex-start'}}><TextField bind={item} name="commissionRate2"/><span style={{lineHeight:"1.5rem",padding:".5rem"}}>%</span></td>
            </tr>
            <tr>
                <td cols={2}><h3>返利</h3></td>
            </tr>
            <tr>
                <td>下级订单总额每</td>
                <td style={{display:"flex",justifyContent:'flex-start'}}>
                    <TextField bind={item} name="perOrderAmount"/><span style={{lineHeight:"1.5rem",padding:".5rem"}}>返利</span><TextField bind={item} name="perOrderRebate"/><span style={{lineHeight:"1.5rem",padding:".5rem"}}>金额</span>
                </td>
            </tr>
        </table>)
    }

    render(){
        let FormContent=this.formContent;
        return (<Vbox css="abs-layout">
            <WindowManager ref={(wm)=>this.win=wm}/>
            <NaviCard title="系统设置" dataSource={["首页","系统管理","系统设置"]} description="二级分销系统执行之前，需完成以下设置"></NaviCard>
            <Card css="mg4" style={{height:"100%"}}>
                <Vbox css="abs-layout">

                    <Form dataSource={this.state.dataSource} ref="form">
                        <FormContent/>
                        <div className="form-command" style={{justifyContent:'center'}}>
                            <Button css="btn-primary"  type="button" onClick={(e)=>{this.save()}} margin="mg1-right">保 存</Button>
                        </div>
                    </Form>
                </Vbox>
            </Card>
        </Vbox>)
    }
}