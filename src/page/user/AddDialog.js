/**
 * Created by quxiangqian on 2018/6/17.
 */
import {Dialog} from 'trilobita-d/control/Dialog';
import {Form} from 'trilobita-d/form/Form';
import {Button} from 'trilobita-d/control/Button';
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {Radio} from "trilobita-d//form/Radio";
import {DaoFactory} from "config/DaoFactory"


export class AddDialog extends React.Component{

    state={
        isupdate:false,
        dataSource:{
            username:{value:"",validate:"null"},
            password:{value:"",validate:"null"},
            realname:{value:"",validate:"null"}
        }
    }

    constructor(){
        super();
        DaoFactory.AdminUser.Add.addSuccess(this.addSuccess)
        DaoFactory.AdminUser.Update.addSuccess(this.addSuccess)
    }

    addSuccess=(data)=>{
        if(this.props.saveAfter){
            this.props.saveAfter();
            this.hide();
        }
    }

    show=(row)=>{
        this.refs.dialog.show();
        if(row){
            this.state.isupdate=true;
            for(let r in row){
                if(this.state.dataSource[r]){
                    this.state.dataSource[r].value=row[r]+"";
                }
            }
        }else {
            this.state.isupdate=false;
            for(let r in this.state.dataSource){
                if(this.state.dataSource[r]){
                    this.state.dataSource[r].value="";
                }
            }
        }

        this.setState( {dataSource:this.state.dataSource});
    }

    hide=()=>{
        this.refs.dialog.hide();
    }


    handleChange(e){

    }


    formContent(props){

        let item=props.dataSource;

        return (<table className="form-layout">
            <tr>
                <td>用户名：</td>
                <td><TextField bind={item} name="username"/></td>
            </tr>
            <tr>
                <td>密码：</td>
                <td><TextField bind={item} name="password"/></td>
            </tr>
            <tr>
                <td>真实姓名：</td>
                <td><TextField bind={item} name="realname"/></td>
            </tr>


        </table>)
    }

    validate=()=>{
        if(this.refs.form.isValiData()){
            if(this.state.isupdate){
                DaoFactory.AdminUser.Update.data=this.refs.form.getData();
                DaoFactory.AdminUser.Update.load();
            }else{
                DaoFactory.AdminUser.Add.data=this.refs.form.getData();
                DaoFactory.AdminUser.Add.load();
            }

        }

    }

    render(){

        let FormContent=this.formContent;
        return (
            <Dialog title={this.state.isupdate?"修改用户":"新增用户"} ref="dialog" {...this.props} >
                <Form dataSource={this.state.dataSource} ref="form">
                    <FormContent/>
                    <div className="form-command">
                        <Button css="btn-primary"  type="button" onClick={(e)=>{this.validate()}} margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={this.hide} margin="mg1-right">关闭</Button>
                    </div>
                </Form>
            </Dialog>
        )
    }
}