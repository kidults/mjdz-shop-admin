
import {DataGrid} from 'trilobita-d/control/DataGrid'
import {Button} from "trilobita-d/control/Button"
import {Panel} from "trilobita-d/control/Panel"
import {Hbox} from "trilobita-d/layout/Hbox"
import {Form} from 'trilobita-d/form/Form'
import {AddDialog} from "./AddDialog"
import {Card} from "trilobita-d/control/Card"
import {NaviCard} from "trilobita-d/control/NaviCard"
import {Vbox} from "trilobita-d/layout/Vbox"
import {PerPageBar} from "trilobita-d/control/PerPageBar"
import {WindowManager} from "trilobita-d/control/WindowManager"
import {Popover} from "trilobita-d/control/Popover"
import {TextField} from "trilobita-d/form/TextField"
import {DateTimeField} from "trilobita-d/form/DateTimeField"
import {ComBox} from "trilobita-d/form/ComBox"
import {Option} from "trilobita-d/form/Option"
import {DaoFactory} from "config/DaoFactory"
import {Dialog} from 'trilobita-d/control/Dialog'


export default class Index extends React.Component{

    constructor(){
        super();
        DaoFactory.AdminUser.List.addSuccess(this.List_Success)
        this.loadList();
    }
    state={
        totalRecord:0,
        dataSource:[

        ],
        idCardDetail:{
            name:'',
            fphto:'',
            bphto:'',
        },
        filterForm:{
            sn:{value:""},
            pageNumber:{value:1},
            pageSize:{value:15},
        }
    }

    List_Success=(data)=>{

        this.state.totalRecord=data.totalRecord;
        this.state.dataSource=data.data;

        this.setState({...this.state.dataSource});
    }

    loadList=()=>{
        let data={};
        for(let key in this.state.filterForm){
            data[key]=this.state.filterForm[key].value;
        }

        DaoFactory.AdminUser.List.data={...data}
        DaoFactory.AdminUser.List.load();
    }

    getCloumn=()=>{
        return [
            {name:'userid',title:'编号',width:'120px'},
            {name:'username',title:'用户名',width:'150px'},
            {name:'realname',title:'真实姓名',width:'150px'},
            {name:'state',title:'状态',width:'150px'},
            {name:'state',title:'操作',width:'150px',render:(row)=>{

                return <a href="javascript:void(0)" onClick={(e)=>{
                    this.showDialog(row);
                }
                }>修改</a>
            }}
        ];
    }


    componentDidMount(){

    }

    formContent(props){

        let item=props.dataSource;
        return (<table className="form-layout" style={{minWidth:'297px',margin:0,marginBottom:'1rem'}}>
            <tr>
                <td>订单号：</td>
                <td></td>
            </tr>
            <tr>
                <td>支付状态：</td>
                <td>

                </td>
            </tr>
        </table>)
    }

    showDialog=(row)=>{
        this.addDialog.show(row);
    }


    render(){
        let FormContent=this.formContent;
        return <Vbox css="abs-layout">
            <Popover title="筛选" ref={(c)=>{this.filterForm=c}} body={
                <Form dataSource={this.state.filterForm} style={{margin:0}} >
                    <FormContent/>
                    <div className="form-command">
                        <Button css="btn-primary"   type="button"  onClick={()=>{
                            this.loadList();
                            this.filterForm.hide();
                        }}  margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={()=>this.filterForm.hide()} margin="mg1-right">关闭</Button>
                    </div>
                </Form>
            } pos="left-top">
                <i class="fas fa-filter" style={{fontSize:16,color:"#666",right:'1rem',top:'7rem',borderRadius:'0.5rem', border:"solid 1px #ccc",padding:'0.5rem', position:"fixed",cursor:"pointer"}}></i>
            </Popover>
            <AddDialog ref={(dialog)=>this.addDialog=dialog} saveAfter={()=>{
                this.loadList();
            }}/>
            <WindowManager ref="wm"/>


            <NaviCard title="用户管理" dataSource={["首页","系统管理","用户管理"]} description="根据条件查询会员情况"></NaviCard>

            <Card css="mg4" style={{height:"100%"}}>
                <Vbox css="abs-layout">
                    <Hbox css="mg1-bottom">
                        <Button css="btn-primary" margin="mg1-right" onClick={(e)=>{
                            this.showDialog()
                        }}>新增</Button>
                    </Hbox>
                    <DataGrid ref="dg" cloumn={this.getCloumn()} dataSource={this.state.dataSource}></DataGrid>
                    <PerPageBar pageSize={this.state.filterForm.pageSize.value}
                                total={this.state.totalRecord}
                                goTo={(index,start,end,pagesize)=>{
                                    this.state.filterForm.pageNumber.value=index;
                                    this.state.filterForm.pageSize.value=pagesize;
                                    this.loadList();
                                }
                                }
                                style={{minHeight:"2.4rem",marginTop:'1rem'}}/>
                </Vbox>
            </Card>
        </Vbox>
    }
}