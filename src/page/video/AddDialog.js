/**
 * Created by quxiangqian on 2018/6/17.
 */
import {Dialog} from 'trilobita-d/control/Dialog';
import {Form} from 'trilobita-d/form/Form';
import {Button} from 'trilobita-d/control/Button';
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {Radio} from "trilobita-d//form/Radio";
import {DaoFactory} from "config/DaoFactory"
import {UploadButton} from "../comm/UploadButton";


export class AddDialog extends React.Component{

    state={
        isupdate:false,
        upload:false,
        process:0,
        dataSource:{
            title:{value:"",validate:"null"},
            content:{value:"",validate:"null"},
            videoUrl:{value:"",validate:"null"},
            coverUrl:{value:"",validate:"null"},
        }
    }

    constructor(){
        super();
        DaoFactory.Video.Add.addSuccess(this.addSuccess)
        DaoFactory.Video.Update.addSuccess(this.addSuccess)
    }

    addSuccess=(data)=>{
        if(this.props.saveAfter){
            this.props.saveAfter();
            this.hide();
        }
    }

    show=(row)=>{
        this.refs.dialog.show();
        if(row){
            this.state.isupdate=true;
            for(let r in row){
                if(this.state.dataSource[r]){
                    this.state.dataSource[r].value=row[r]+"";
                }
            }
            this.state.dataSource.id={value:row.id};
        }else {
            this.state.isupdate=false;
            for(let r in this.state.dataSource){
                if(this.state.dataSource[r]){
                    this.state.dataSource[r].value="";
                }
            }
            delete this.state.dataSource["id"];
        }

        this.setState( {dataSource:this.state.dataSource});
    }

    hide=()=>{
        this.refs.dialog.hide();
    }


    handleChange(e){

    }

    proccess=(b,n)=>{
        this.state.upload=b;
        this.state.process=n;
        this.setState({...this.state});
    }

    formContent=(props)=>{

        let item=props.dataSource;

        return (<table className="form-layout">
            <tr>
                <td>标题：</td>
                <td><TextField bind={item} name="title"/></td>
            </tr>
            <tr>
                <td>简介：</td>
                <td><TextField bind={item} name="content"/></td>
            </tr>
            <tr>
                <td>视频封面：</td>
                <td><TextField bind={item} name="coverUrl" /></td>
                <td><UploadButton success={(s)=>{
                    item["coverUrl"].value=s;
                    this.setState({...this.state})
                }} proccess={this.proccess} >上传</UploadButton></td>
            </tr>
            <tr>
                <td>视频：</td>
                <td><TextField bind={item} name="videoUrl"/></td>
                <td><UploadButton type="video" success={(s)=>{
                    item["videoUrl"].value=s;
                    this.setState({...this.state})
                }} proccess={this.proccess} >上传</UploadButton></td>
            </tr>
        </table>



        )
    }

    validate=()=>{
        if(this.refs.form.isValiData()){
            if(this.state.isupdate){
                DaoFactory.Video.Update.data=this.refs.form.getData();
                DaoFactory.Video.Update.load();
            }else{
                DaoFactory.Video.Add.data=this.refs.form.getData();
                DaoFactory.Video.Add.load();
            }

        }

    }

    render(){

        let FormContent=this.formContent;
        return (
            <Dialog title={this.state.isupdate?"修改视频":"新增视频"} ref="dialog" {...this.props} >
                <Form dataSource={this.state.dataSource} ref="form">
                    <FormContent/>
                    <div>
                    {this.state.upload ?
                        <div style={{background:"#c3c3c3",margin:"1rem"}}>
                            <div style={{background:"#81cb92",width:this.state.process+"%",display:"block",textAlign:"center"}}>{this.state.process}%</div>
                        </div>
                        :""}
                    </div>
                    <div className="form-command">
                        <Button css="btn-primary"  type="button" onClick={(e)=>{this.validate()}} margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={this.hide} margin="mg1-right">关闭</Button>
                    </div>
                </Form>
            </Dialog>
        )
    }
}