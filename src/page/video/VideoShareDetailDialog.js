/**
 * Created by quxiangqian on 2018/6/17.
 */
import {Dialog} from 'trilobita-d/control/Dialog';
import {Form} from 'trilobita-d/form/Form';
import {Button} from 'trilobita-d/control/Button';
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {Radio} from "trilobita-d/form/Radio";
import {DaoFactory} from "config/DaoFactory"
import {DataGrid} from 'trilobita-d/control/DataGrid'
import {Card} from "trilobita-d/control/Card";
import {Vbox} from "trilobita-d/layout/Vbox";
import {PerPageBar} from "trilobita-d/control/PerPageBar";

export class VideoShareDetailDialog extends React.Component{

    state={
        totalRecord:0,
        dataSource:[

        ],

        filterForm:{
            videoId:{value:""},
            pageNumber:{value:1},
            pageSize:{value:15},
        }

    }
    constructor(){
        super();
        DaoFactory.VideoShare.List.addSuccess(this.listSuccess)

    }





    listSuccess=(data)=>{
        this.state.dataSource=data.data;
        this.state.totalRecord=data.totalRecord;
        this.setState({dataSource:this.state.dataSource});
    }

    show=(row)=>{
        this.state.filterForm.videoId.value=row.id;
        this.load();
        this.refs.dialog.show();
    }

    load=()=>{
        let data={};
        for(let key in this.state.filterForm){
            data[key]=this.state.filterForm[key].value;
        }
        DaoFactory.VideoShare.List.data=data;
        DaoFactory.VideoShare.List.load();
    }

    hide=()=>{
        this.refs.dialog.hide();
    }


    handleChange(e){

    }





    getCloumn=()=>{
        return [
            {name:'uname',title:'用户名',width:'120px'},
            {name:'createDate',title:'分享时间',width:'250px'}
        ];
    }





    render(){


        return (
            <Dialog title="视频分享情况" ref="dialog" {...this.props} >

                    <Card style={{height:300,width:480,position:'relative'}}>
                        <Vbox css="abs-layout">
                            {/*<Form dataSource={this.state.filterForm} ref="form">*/}
                                {/*<FormContent></FormContent>*/}
                            {/*</Form>*/}
                            <DataGrid ref="dg" cloumn={this.getCloumn()} dataSource={this.state.dataSource}></DataGrid>
                            <PerPageBar pageSize={this.state.filterForm.pageSize.value}
                                        total={this.state.totalRecord}
                                        goTo={(index,start,end,pagesize)=>{
                                            this.state.filterForm.pageNumber.value=index;
                                            this.state.filterForm.pageSize.value=pagesize;
                                            this.load();
                                        }
                                        }
                                        style={{minHeight:"2.4rem",marginTop:'1rem'}}/>
                        </Vbox>
                    </Card>
                    <div className="form-command">
                        <Button css="btn-default"  type="button" onClick={this.hide} margin="mg1-right">关闭</Button>
                    </div>

            </Dialog>
        )
    }
}