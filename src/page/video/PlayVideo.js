/**
 * Created by quxiangqian on 2018/6/17.
 */
import {Dialog} from 'trilobita-d/control/Dialog';
import {Form} from 'trilobita-d/form/Form';
import {Button} from 'trilobita-d/control/Button';
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {Radio} from "trilobita-d//form/Radio";
import {DaoFactory} from "config/DaoFactory"


export class PlayVideo extends React.Component{

    state={
        isupdate:false,
        upload:false,
        process:0,
        dataSource:{
            title:{value:""},
            content:{value:""},
            videoUrl:{value:"",validate:"null"},
            coverUrl:{value:"",validate:"null"},
        }
    }

    constructor(){
        super();
    }


    show=(row)=>{
        this.refs.dialog.show();
        if(row){
            this.state.isupdate=true;
            for(let r in row){
                if(this.state.dataSource[r]){
                    this.state.dataSource[r].value=row[r]+"";
                }
            }
            this.state.dataSource.id={value:row.id};
        }

        this.setState( {dataSource:this.state.dataSource});
    }

    hide=()=>{
        this.refs.dialog.hide();
    }


    handleChange(e){

    }

    proccess=(b,n)=>{
        this.state.upload=b;
        this.state.process=n;
        this.setState({...this.state});
    }



    validate=()=>{
        if(this.refs.form.isValiData()){
            if(this.state.isupdate){
                DaoFactory.Video.Update.data=this.refs.form.getData();
                DaoFactory.Video.Update.load();
            }else{
                DaoFactory.Video.Add.data=this.refs.form.getData();
                DaoFactory.Video.Add.load();
            }

        }

    }

    render(){

        let FormContent=this.formContent;
        return (
            <Dialog title={"视频播放-"+this.state.dataSource.title.value} ref="dialog" {...this.props} >

                    <video width={537} height={300} controls="controls" src={this.state.dataSource.videoUrl.value} poster={this.state.dataSource.coverUrl.value}></video>
                    <div className="form-command">

                        <Button css="btn-default"  type="button" onClick={this.hide} margin="mg1-right">关闭</Button>
                    </div>

            </Dialog>
        )
    }
}