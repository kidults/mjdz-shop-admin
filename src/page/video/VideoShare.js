
import {DataGrid} from 'trilobita-d/control/DataGrid'
import {Button} from "trilobita-d/control/Button"
import {Panel} from "trilobita-d/control/Panel"
import {Hbox} from "trilobita-d/layout/Hbox"
import {Form} from 'trilobita-d/form/Form'
import {Card} from "trilobita-d/control/Card"
import {NaviCard} from "trilobita-d/control/NaviCard"
import {Vbox} from "trilobita-d/layout/Vbox"
import {PerPageBar} from "trilobita-d/control/PerPageBar"
import {WindowManager} from "trilobita-d/control/WindowManager"
import {Popover} from "trilobita-d/control/Popover"
import {TextField} from "trilobita-d/form/TextField"
import {DateTimeField} from "trilobita-d/form/DateTimeField"
import {ComBox} from "trilobita-d/form/ComBox"
import {Option} from "trilobita-d/form/Option"
import {DaoFactory} from "config/DaoFactory"
import {Dialog} from 'trilobita-d/control/Dialog'
import {ToolTip} from "trilobita-d/control/ToolTip"
import {PlayVideo} from "./PlayVideo";

import {VideoShareDetailDialog} from './VideoShareDetailDialog'

export default class VideoShare extends React.Component{

    constructor(){
        super();
        DaoFactory.Video.List.addSuccess(this.List_Success)
        this.loadList();

    }



    state={
        totalRecord:0,
        dataSource:[

        ],
        idCardDetail:{
            name:'',
            fphto:'',
            bphto:'',
        },
        filterForm:{
            keyword:{value:""},
            status:{value:""},
            pageNumber:{value:1},
            pageSize:{value:15},
        }
    }

    List_Success=(data)=>{

        this.state.totalRecord=data.totalRecord;
        this.state.dataSource=data.data;

        this.setState({...this.state.dataSource});
    }

    loadList=()=>{
        let data={};
        for(let key in this.state.filterForm){
            data[key]=this.state.filterForm[key].value;
        }

        DaoFactory.Video.List.data={...data}
        DaoFactory.Video.List.load();
    }

    getCloumn=()=>{
        return [
            {name:'title',title:'标题',render:(row)=>{
                return <span><i class="fas fa-chevron-circle-right" style={{color:"#71ff3c",fontSize:"1rem",marginRight:"1rem"}} onClick={(e)=>{
                    this.playVideoDialog.show(row);
                }}></i>{row.title}</span>
            },width:'320px'},
            {name:'content',title:'简介',width:'350px'},
            {name:'sharedCount',title:'分享次数',width:'150px',render:(row)=>{

                return <a href="javascript:void(0)" onClick={(e)=>{this.videShareDetailDialog.show(row)}}>{row.sharedCount}</a>
            }},


            {name:'status',title:'上下架状态',render:(row)=>{
                if(row.status===1){
                    return "上架"
                }
                if(row.status===0){
                    return "下架"
                }
            },
                width:'150px'},
            {name:'createDate',title:'创建时间',width:"200px"},


        ];
    }


    componentDidMount(){

    }

    formContent(props){

        let item=props.dataSource;
        const {date, format, mode, inputFormat} = {
            date: "1990-06-05",
            format: "YYYY-MM-DD",
            inputFormat: "DD/MM/YYYY",
            mode: "date"
        };
        return (<table className="form-layout" style={{minWidth:'297px',margin:0,marginBottom:'1rem'}}>
            <tr>
                <td>关键词：<ToolTip text="请输入标题,简介的任意关键词"><i class="fas fa-info-circle" style={{fontSize:"1rem"}}></i></ToolTip></td>
                <td><TextField bind={item} name="keyword"/></td>
            </tr>
            <tr>
                <td>上架状态：</td>
                <td>
                    <ComBox readOnly={true} value={item["status"].value} bind={item} name="status">
                        <Option value="">全部</Option>
                        <Option value="1">上架</Option>
                        <Option value="0">下架</Option>
                    </ComBox>
                </td>
            </tr>
        </table>)
    }

    showDialog=(row)=>{
        this.addDialog.show(row);
    }


    render(){
        let FormContent=this.formContent;
        return <Vbox css="abs-layout">
            <Popover title="筛选" ref={(c)=>{this.filterForm=c}} body={
                <Form dataSource={this.state.filterForm} style={{margin:0}} >
                    <FormContent/>
                    <div className="form-command">
                        <Button css="btn-primary"   type="button"  onClick={()=>{
                            this.loadList();
                            this.filterForm.hide();
                        }}  margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={()=>this.filterForm.hide()} margin="mg1-right">关闭</Button>
                    </div>
                </Form>
            } pos="left-top">
                <i class="fas fa-filter" style={{fontSize:16,color:"#666",right:'1rem',top:'7rem',borderRadius:'0.5rem', border:"solid 1px #ccc",padding:'0.5rem', position:"fixed",cursor:"pointer"}}></i>
            </Popover>
            <PlayVideo ref={(dialog)=>this.playVideoDialog=dialog}/>



            <VideoShareDetailDialog ref={(dialog)=>this.videShareDetailDialog=dialog}></VideoShareDetailDialog>

            <WindowManager ref="wm"/>


            <NaviCard title="查看分享记录" dataSource={["首页","视频管理","查看分享记录"]} description="根据条件查询视频情况"></NaviCard>

            <Card css="mg4" style={{height:"100%"}}>
                <Vbox css="abs-layout">
                    <DataGrid ref="dg" cloumn={this.getCloumn()} dataSource={this.state.dataSource}></DataGrid>
                    <PerPageBar pageSize={this.state.filterForm.pageSize.value}
                                total={this.state.totalRecord}
                                goTo={(index,start,end,pagesize)=>{
                                    this.state.filterForm.pageNumber.value=index;
                                    this.state.filterForm.pageSize.value=pagesize;
                                    this.loadList();
                                }
                                }
                                style={{minHeight:"2.4rem",marginTop:'1rem'}}/>
                </Vbox>
            </Card>
        </Vbox>
    }
}