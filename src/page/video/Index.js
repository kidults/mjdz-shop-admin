
import {DataGrid} from 'trilobita-d/control/DataGrid'
import {Button} from "trilobita-d/control/Button"
import {Panel} from "trilobita-d/control/Panel"
import {Hbox} from "trilobita-d/layout/Hbox"
import {Form} from 'trilobita-d/form/Form'
import {AddDialog} from "./AddDialog"
import {Card} from "trilobita-d/control/Card"
import {NaviCard} from "trilobita-d/control/NaviCard"
import {Vbox} from "trilobita-d/layout/Vbox"
import {PerPageBar} from "trilobita-d/control/PerPageBar"
import {WindowManager} from "trilobita-d/control/WindowManager"
import {Popover} from "trilobita-d/control/Popover"
import {TextField} from "trilobita-d/form/TextField"
import {DateTimeField} from "trilobita-d/form/DateTimeField"
import {ComBox} from "trilobita-d/form/ComBox"
import {Option} from "trilobita-d/form/Option"
import {DaoFactory} from "config/DaoFactory"
import {Dialog} from 'trilobita-d/control/Dialog'
import {UpdateStartCountDialog} from './UpdateStartCountDialog'
import {ToolTip} from "trilobita-d/control/ToolTip"

import {VideFavorDetailDialog} from './VideFavorDetailDialog'

export default class Index extends React.Component{

    constructor(){
        super();
        DaoFactory.Video.List.addSuccess(this.List_Success)
        this.loadList();
        DaoFactory.Video.SetHome.addSuccess(this.flushList);
        DaoFactory.Video.UpdateState.addSuccess(this.flushList);
    }

    flushList=(data)=>{
        if(data.code===200){
            this.loadList();
        }
    }

    state={
        totalRecord:0,
        dataSource:[

        ],
        idCardDetail:{
            name:'',
            fphto:'',
            bphto:'',
        },
        filterForm:{
            keyword:{value:""},
            status:{value:""},
            pageNumber:{value:1},
            pageSize:{value:15},
        }
    }

    List_Success=(data)=>{

        this.state.totalRecord=data.totalRecord;
        this.state.dataSource=data.data;

        this.setState({...this.state.dataSource});
    }

    loadList=()=>{
        let data={};
        for(let key in this.state.filterForm){
            data[key]=this.state.filterForm[key].value;
        }

        DaoFactory.Video.List.data={...data}
        DaoFactory.Video.List.load();
    }

    getCloumn=()=>{
        return [
            {name:'title',title:'标题',render:(row)=>{
                let str=null
                if(row.isHome===1){
                    str=<span style={{color:"#232672"}}>{"-[显示到首页]"}</span>
                }
                return <span>{row.title}{str}</span>
            },width:'320px'},
            {name:'content',title:'简介',width:'350px'},
            {name:'favorCount',title:'真实点赞次数',width:'150px',render:(row)=>{

                return <a href="javascript:void(0)" onClick={(e)=>{this.videFavorDetailDialog.show(row)}}>{row.favorCount}</a>
            }},
            {name:'startCount',title:'虚拟点赞次数',width:'150px',render:(row)=>{

                return <a href="javascript:void(0)" onClick={(e)=>{this.updateStartCountDialog.show(row)}}>{row.startCount}</a>
            }},
            {name:'sharedCount',title:'被分享次数',width:'150px'},

            {name:'status',title:'上下架状态',render:(row)=>{
               if(row.status===1){
                   return "上架"
               }
                if(row.status===0){
                    return "下架"
                }
            },
                width:'150px'},
            {name:'createDate',title:'创建时间',width:"200px"},

            {name:'state',title:'操作',width:'250px',render:(row)=>{

                return <span>
                    <a href="javascript:void(0)" onClick={(e)=>{
                    this.showDialog(row);
                    }
                    }>修改</a>
                    &nbsp;| &nbsp;
                     <a href="javascript:void(0)" onClick={(e)=>{
                         DaoFactory.Video.SetHome.data={id:row.id}
                         DaoFactory.Video.SetHome.load();
                     }
                     }>显示到首页</a> &nbsp;| &nbsp;
                    <a href="javascript:void(0)" onClick={(e)=>{
                        let stat=0;
                        if(row.status===0){
                            stat=1;
                        }
                        DaoFactory.Video.UpdateState.data={id:row.id,status:stat}
                        DaoFactory.Video.UpdateState.load();
                    }
                    }>上下架</a>
                </span>
            }}
        ];
    }


    componentDidMount(){

    }

    formContent(props){

        let item=props.dataSource;
        const {date, format, mode, inputFormat} = {
            date: "1990-06-05",
            format: "YYYY-MM-DD",
            inputFormat: "DD/MM/YYYY",
            mode: "date"
        };
        return (<table className="form-layout" style={{minWidth:'297px',margin:0,marginBottom:'1rem'}}>
            <tr>
                <td>关键词：<ToolTip text="请输入标题,简介的任意关键词"><i class="fas fa-info-circle" style={{fontSize:"1rem"}}></i></ToolTip></td>
                <td><TextField bind={item} name="keyword"/></td>
            </tr>
            <tr>
                <td>上架状态：</td>
                <td>
                    <ComBox readOnly={true} value={item["status"].value} bind={item} name="status">
                        <Option value="">全部</Option>
                        <Option value="1">上架</Option>
                        <Option value="0">下架</Option>
                    </ComBox>
                </td>
            </tr>
        </table>)
    }

    showDialog=(row)=>{
        this.addDialog.show(row);
    }


    render(){
        let FormContent=this.formContent;
        return <Vbox css="abs-layout">
            <Popover title="筛选" ref={(c)=>{this.filterForm=c}} body={
                <Form dataSource={this.state.filterForm} style={{margin:0}} >
                    <FormContent/>
                    <div className="form-command">
                        <Button css="btn-primary"   type="button"  onClick={()=>{
                            this.loadList();
                            this.filterForm.hide();
                        }}  margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={()=>this.filterForm.hide()} margin="mg1-right">关闭</Button>
                    </div>
                </Form>
            } pos="left-top">
                <i class="fas fa-filter" style={{fontSize:16,color:"#666",right:'1rem',top:'7rem',borderRadius:'0.5rem', border:"solid 1px #ccc",padding:'0.5rem', position:"fixed",cursor:"pointer"}}></i>
            </Popover>
            <AddDialog ref={(dialog)=>this.addDialog=dialog} saveAfter={()=>{
                this.loadList();
            }}/>
            <UpdateStartCountDialog ref={(dialog)=>this.updateStartCountDialog=dialog} saveAfter={()=>{
                this.loadList();
            }}></UpdateStartCountDialog>

            <VideFavorDetailDialog ref={(dialog)=>this.videFavorDetailDialog=dialog}></VideFavorDetailDialog>

            <WindowManager ref="wm"/>


            <NaviCard title="视频管理" dataSource={["首页","视频管理","视频管理"]} description="根据条件查询视频情况"></NaviCard>

            <Card css="mg4" style={{height:"100%"}}>
                <Vbox css="abs-layout">
                    <Hbox css="mg1-bottom">
                        <Button css="btn-primary" margin="mg1-right" onClick={(e)=>{
                            this.showDialog()
                        }}>新增</Button>
                    </Hbox>
                    <DataGrid ref="dg" cloumn={this.getCloumn()} dataSource={this.state.dataSource}></DataGrid>
                    <PerPageBar pageSize={this.state.filterForm.pageSize.value}
                                total={this.state.totalRecord}
                                goTo={(index,start,end,pagesize)=>{
                                    this.state.filterForm.pageNumber.value=index;
                                    this.state.filterForm.pageSize.value=pagesize;
                                    this.loadList();
                                }
                                }
                                style={{minHeight:"2.4rem",marginTop:'1rem'}}/>
                </Vbox>
            </Card>
        </Vbox>
    }
}