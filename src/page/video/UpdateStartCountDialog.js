/**
 * Created by quxiangqian on 2018/6/17.
 */
import {Dialog} from 'trilobita-d/control/Dialog';
import {Form} from 'trilobita-d/form/Form';
import {Button} from 'trilobita-d/control/Button';
import {TextField} from "trilobita-d/form/TextField";
import {DateTimeField} from "trilobita-d/form/DateTimeField";
import {ComBox} from "trilobita-d/form/ComBox";
import {Option} from "trilobita-d/form/Option";
import {Radio} from "trilobita-d//form/Radio";
import {DaoFactory} from "config/DaoFactory"


export class UpdateStartCountDialog extends React.Component{

    state={
        isupdate:false,
        upload:false,
        process:0,
        dataSource:{
            startCount:{value:"",validate:"null"},
            id:{value:""},
        }
    }

    constructor(){
        super();
        console.log("初始化。。")
        DaoFactory.Video.UpdateStartCount.addSuccess(this.addSuccess)
    }

    addSuccess=(data)=>{
        if(this.props.saveAfter){
            this.props.saveAfter();
            if(this.hide){
                this.hide();
            }

        }
    }

    show=(row)=>{
        this.refs.dialog.show();
        if(row){
            this.state.isupdate=true;
            for(let r in row){
                if(this.state.dataSource[r]){
                    this.state.dataSource[r].value=row[r]+"";
                }
            }
        }

        this.setState( {dataSource:this.state.dataSource});
    }

    hide=()=>{
        if(this.refs.dialog){
            this.refs.dialog.hide();
        }
    }


    handleChange(e){

    }



    formContent=(props)=>{

        let item=props.dataSource;

        return (<table className="form-layout">

            <tr>
                <td>数量：</td>
                <td><TextField bind={item} name="startCount"/></td>
            </tr>

        </table>



        )
    }

    validate=()=>{
        if(this.refs.form.isValiData()){

                DaoFactory.Video.UpdateStartCount.data=this.refs.form.getData();
                DaoFactory.Video.UpdateStartCount.load();


        }

    }

    render(){

        let FormContent=this.formContent;
        return (
            <Dialog title={"修改视频虚拟点赞数量"} ref="dialog" {...this.props} >
                <Form dataSource={this.state.dataSource} ref="form">
                    <FormContent/>

                    <div className="form-command">
                        <Button css="btn-primary"  type="button" onClick={(e)=>{this.validate()}} margin="mg1-right">确定</Button>
                        <Button css="btn-default"  type="button" onClick={this.hide} margin="mg1-right">关闭</Button>
                    </div>
                </Form>
            </Dialog>
        )
    }
}