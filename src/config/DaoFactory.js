/**
 * Created by quxiangqian on 2018/7/5.
 */
import {DataSource} from 'trilobita-d/data/DataSource'
//基础链接
// let baseUrl="https://mingjiu-api.conpanda.cn/front_v1";
// let baseUrl1="https://mingjiu-api.conpanda.cn/admin_v1";
let baseUrl="https://mjapi.conpanda.cn/front_v1";
let baseUrl1="https://mjapi.conpanda.cn/admin_v1";

const Order_OrderFind=new DataSource();
Order_OrderFind.url=baseUrl+"/EsCustomizationOrder/FindAll"

const Order_UpdateShipStatus=new DataSource();
Order_UpdateShipStatus.url=baseUrl1+"/customizationOrder/updateShipStatus"
const Order_UpdateShipInfo=new DataSource();
Order_UpdateShipInfo.url=baseUrl1+"/customizationOrder/updateShipInfo"


const Member_IdCardFind=new DataSource();
Member_IdCardFind.url=baseUrl1+"/member/idcard/list"

const Member_Audit=new DataSource();
Member_Audit.url=baseUrl+"/admin/memberidcard/check"

const Member_list=new DataSource();
Member_list.url=baseUrl1+"/member/list"

//全部经销商
const Dealer_list=new DataSource();
Dealer_list.url=baseUrl1+"/dealer/list"
//待审核经销商
const Dealer_auditNot_list=new DataSource();
Dealer_auditNot_list.url=baseUrl1+"/dealer/auditNot/list"
const DealerAuditHistory=new DataSource();
DealerAuditHistory.url=baseUrl1+"/authorizeRecord/findByMemberId"
    //authorizeRecord/findByMemberId
//未通过经销商
const Dealer_auditFail_list=new DataSource();
Dealer_auditFail_list.url=baseUrl1+"/dealer/auditFail/list"

const Dealer_Authorize=new DataSource();
Dealer_Authorize.url=baseUrl1+"/dealer/authorize"


//系统设置
const SystemConfig_One=new DataSource();
SystemConfig_One.url=baseUrl1+"/systemconfig/one"
const SystemConfig_Records=new DataSource();
SystemConfig_Records.url=baseUrl1+"/systemconfig/records"


const SystemConfig_Update=new DataSource();
SystemConfig_Update.url=baseUrl1+"/systemconfig/update"

const AdminUser_Login=new DataSource();
AdminUser_Login.url=baseUrl1+"/adminuser/login"

const AdminUser_List=new DataSource();
AdminUser_List.url=baseUrl1+"/adminuser/list"

const AdminUser_Add=new DataSource();
AdminUser_Add.url=baseUrl1+"/adminuser/add"
const AdminUser_Update=new DataSource();
AdminUser_Update.url=baseUrl1+"/adminuser/update"


// /adminuser/add

//交易记录
const TradeRecord_List=new DataSource();
TradeRecord_List.url=baseUrl1+"/tradeRecord/list"

const TradeRecord_StatInfo=new DataSource();
TradeRecord_StatInfo.url=baseUrl1+"/tradeRecord/statInfo"
const TradeRecord_Detail=new DataSource();
TradeRecord_Detail.url=baseUrl1+"/RsTradeDetail/FindDetail"

const WithdrawRecordList=new DataSource();
WithdrawRecordList.url=baseUrl1+"/withdrawRecord/list"
    //withdrawRecord/updateWithdrawState
const WithdrawUpdateState=new DataSource();
WithdrawUpdateState.url=baseUrl1+"/withdrawRecord/updateWithdrawState"
//基础数据
//交易记录
const BaseData_Tradetypes=new DataSource();
BaseData_Tradetypes.url=baseUrl1+"/BaseData/tradetypes"

//视频管理

const Video_List=new DataSource();
Video_List.url=baseUrl1+"/video/list"
const Video_FindById=new DataSource();
Video_FindById.url=baseUrl1+"/video/findById"
const Video_Add=new DataSource();
Video_Add.url=baseUrl1+"/video/add"

const Video_SetHome=new DataSource();
Video_SetHome.url=baseUrl1+"/video/setHome"
const Video_Update=new DataSource();
Video_Update.url=baseUrl1+"/video/update"

const Video_UpdateStartCount=new DataSource();
Video_UpdateStartCount.url=baseUrl1+"/video/updateStartCount"

const Video_UpdateStatus=new DataSource();
Video_UpdateStatus.url=baseUrl1+"/video/updateStatus"

const VideoFavor_list=new DataSource();
VideoFavor_list.url=baseUrl1+"/videoFavor/list"
const VideoShare_list=new DataSource();
VideoShare_list.url=baseUrl1+"/videoShare/list"

///qrcode/saveLogo

const Qrcode_SaveLogo=new DataSource();
Qrcode_SaveLogo.url=baseUrl1+"/qrcode/saveLogo"
/**
 * 数据访问工厂
 */
export class DaoFactory{
    /**
     * 订单接口
     * @type {{OrderFind: DataSource}}
     */
    static Order={
        OrderFind:Order_OrderFind,
        UpdateShipStatus:Order_UpdateShipStatus,
        UpdateShipInfo:Order_UpdateShipInfo,
    }
    /**
     * 会员接口
     * @type {{IdCardFind: DataSource, Audit: DataSource}}
     */
    static Member={
        IdCardFind:Member_IdCardFind,
        Audit:Member_Audit,
        List:Member_list,
    }

    static Dealer={
       List:Dealer_list,
       AuditNotList:Dealer_auditNot_list,
       AuditFailList: Dealer_auditFail_list,
       Authorize:Dealer_Authorize,
       AuditHistory:DealerAuditHistory,
    }
    static SystemConfig={
        One:SystemConfig_One,
        Update:SystemConfig_Update,
        His:SystemConfig_Records,
    }

    static AdminUser={
        Login:AdminUser_Login,
        List:AdminUser_List,
        Add:AdminUser_Add,
        Update:AdminUser_Update,
    }
    static Finance={
        TradeRecordList:TradeRecord_List,
        TradeRecordStatInfo:TradeRecord_StatInfo,
        WithDrawList:WithdrawRecordList,
        WithdrawUpdateState:WithdrawUpdateState,
        TradeDetail:TradeRecord_Detail
    }

    static BaseData={
        Tradetypes:BaseData_Tradetypes
    }

    static Video={
        List:Video_List,
        Add:Video_Add,
        Update:Video_Update,
        UpdateStartCount:Video_UpdateStartCount,
        FindById:Video_FindById,
        SetHome:Video_SetHome,
        UpdateState:Video_UpdateStatus,
    }
    static VideoFavor={
        List:VideoFavor_list
    }
    static VideoShare={
        List:VideoShare_list
    }
    static QrCode={
        PlatForm:baseUrl1+"/qrcode/platform",
        SaveLogo:Qrcode_SaveLogo,
    }
}