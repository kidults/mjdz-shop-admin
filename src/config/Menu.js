/**
 * Created by quxiangqian on 2018/7/3.
 */
module.exports=[
    {id:1,parent:0,name:"系统管理",path:"",ico:"fas fa-cog"},
    {id:1001,parent:1,name:"首页",path:"page/Index"},
    {id:1002,parent:1,name:"用户管理",path:"page/user/Index"},
    {id:1003,parent:1,name:"平台二维码",path:"page/platform/Index"},
    {id:2,parent:0,name:"会员管理",path:"",ico:"fas fa-users"},
    {id:2001,parent:2,name:"会员查询",path:"page/member/Member"},
    {id:2002,parent:2,name:"实名认证",path:"page/member/IdCard"},
    {id:3,parent:0,name:"经销商管理",path:"",ico:"fas fa-users-cog"},
    {id:3001,parent:3,name:"待审核",path:"page/dealer/Wait"},
    {id:3003,parent:3,name:"未通过",path:"page/dealer/NotAudit"},
    {id:3004,parent:3,name:"全部经销商",path:"page/dealer/All"},
    {id:4,parent:0,name:"订单管理",path:"",ico:"fab fa-wpforms"},
    {id:4001,parent:4,name:"待处理",path:"page/order/Wait"},
    {id:4002,parent:4,name:"已发货",path:"page/order/Shipped"},
    {id:4003,parent:4,name:"已完成",path:"page/order/Complete"},
    {id:4003,parent:4,name:"全部订单",path:"page/order/Index"},
    {id:5,parent:0,name:"财务管理",path:"",ico:"fas fa-yen-sign"},
    {id:50001,parent:5,name:"经销商收益",path:"page/finance/Income"},
    {id:50002,parent:5,name:"经销商提现待处理",path:"page/finance/WithDrawWait"},
    {id:50003,parent:5,name:"经销商提现完成",path:"page/finance/WithDrawSuccess"},
    {id:50004,parent:5,name:"经销商提现失败",path:"page/finance/WithDrawFail"},

    {id:6,parent:0,name:"视频管理",path:"",ico:"fas fa-video"},
    {id:6001,parent:6,name:"视频上传",path:"page/video/UploadVideoList"},
    {id:6002,parent:6,name:"视频上下架",path:"page/video/VideoStatus"},
    {id:6003,parent:6,name:"首页视频",path:"page/video/VideoHome"},
    {id:6004,parent:6,name:"设置虚拟点赞",path:"page/video/VideoStartCount"},
    {id:6005,parent:6,name:"查看点赞记录",path:"page/video/VideoFavor"},
    {id:6005,parent:6,name:"查看分享记录",path:"page/video/VideoShare"},

]