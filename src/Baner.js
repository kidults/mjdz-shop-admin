/**
 * Created by quxiangqian on 2018/7/13.
 */
export class Baner extends React.Component{
    state={
        isExp:false
    }
    render(){
        let userinfo=JSON.parse(sessionStorage.getItem("userInfo"));
        console.log(userinfo.username);
        let expclass="far fa-arrow-alt-circle-right";
        if(!this.state.isExp){
            expclass="far fa-arrow-alt-circle-left";
        }
        return (<div className="subbaner">
            <div className="left">
                <i class={expclass} onMouseDown={(e)=>{
                    this.state.isExp=!this.state.isExp;
                    if(this.props.toggle){
                        this.props.toggle(this.state.isExp);
                    }
                    this.setState({isExp:this.state.isExp});
                }}> </i>
            </div>
            <div className="right">
                <span>你好，用户{userinfo.username}欢迎使用本系统</span>
                <i class="fas fa-sign-out-alt" style={{fontSize:"1rem",margin:"1.1rem"}} onClick={(e)=>{
                      if(this.props.unlogin){
                          this.props.unlogin();
                      }
                   }
                }></i>
            </div>
        </div>
        )
    }
}