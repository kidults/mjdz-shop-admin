/**
 * Created by quxiangqian on 2018/6/5.
 */

import {Router} from 'trilobita-d/control/Router'
import Menu from 'config/Menu'
import {AccordionMenu} from 'trilobita-d/control/AccordionMenu'
import {AdminDefaultLayout} from 'trilobita-d/layout/AdminDefaultLayout'
import {Login} from "./Login";
import {Baner} from "./Baner";
export default class App extends React.Component{

    state={
        isLogin:false
    }

    accordionMenuItemClick=(m)=>{
        location.href="#"+m.path;
        this.refs.router.load(m.path);
        this.refs.amenu.active(m.path);
    }

    routerInitLoad=(path)=>{

        this.refs.amenu.active(path);

    }

    goto=()=>{
        location.href="./#page/Index"
        this.setState({load:true});
        //this.refs.router.load("page/Index");
        //this.refs.amenu.active("page/Index");
    }

    render() {
        //console.log(this)
        let path = location.hash;
        if (!path || path === "") {
            path = "#page/Index"
        }else if (path === "#login") {
            sessionStorage.removeItem("isLogin")
        }
        // let temppath=path.replace("#","").split("/");
        let hashpath = path.replace("#", "");
        if(sessionStorage.getItem("isLogin")){
            this.state.isLogin=true;
        }else{
            location.href="#login";
            this.state.isLogin=false;
        }
        return (
            this.state.isLogin?
            <AdminDefaultLayout>
                <div region="leftbaner" className="left-baner">

                </div>

                <Baner region="baner" unlogin={(e)=>{
                    location.href="./#login"
                    this.setState({load:true});
                }}
                 toggle={
                     (b)=>{
                         if(b){
                             $(".admin-default-layout >.left").hide();
                         }else{
                             $(".admin-default-layout >.left").show();
                         }
                     }
                 }
                />
                <AccordionMenu ref="amenu" style={{display:'none'}} currPath={hashpath} region="left" dataSource={Menu}
                               itemClick={this.accordionMenuItemClick}></AccordionMenu>
                <Router ref="router" region="center" initLoad={this.routerInitLoad}></Router>
            </AdminDefaultLayout>:<Login goto={this.goto}/>
        )
    }
}